using System;
using System.Net;
using System.Threading;
using System.Net.Sockets;
using System.Linq;
using System.Text;

/* 
 * Actual client handler class that makes the game go the way it's supposed to.
 * Does a lot of things, like send and receive data when it needs to
 * Setup for ships, decide when someone has been hit or not, decide when game
 * is over, etc.
 * zbwells and crew
 * Nov 2023
 */

using static SharedInfo;

class HandleClient
{
    private TcpClient clientSocket { get; set; }
    public SharedInfo shared;
    public readonly int clientID;
    
    private string respstr { get; set; }
    private string recvstr { get; set; }

    /* Set up data, and then start a thread for the server to handle a client */
    public HandleClient(TcpClient client, int i, ref SharedInfo s)
    {
        clientID = i;
        clientSocket = client;
        shared = s;
        respstr = "";
        recvstr = "";
        
        Thread cthread = new(playGame);
        cthread.Start();
    }

    /* Send string to client */
    private void sendString(ref NetworkStream sm, string str)
    {
        byte[] sendBytes = new byte[1024];
        sendBytes = Encoding.UTF8.GetBytes(str);
        sm.Write(sendBytes, 0, sendBytes.Length);
        sm.Flush();    
    }

    /* Receive string from client */
    private string receiveString(ref NetworkStream sm)
    {
        byte[] bytesFrom = new byte[1024];
        int received = sm.Read(bytesFrom);
        return Encoding.UTF8.GetString(bytesFrom, 0, received).ToUpper();    
    }

    /* Have one client wait for the other to connect */
    private void waitConnect(ref NetworkStream sm)
    {
        if (shared.mode == State.LISTENING)
        {
            respstr = "! Waiting for other player to connect.\n";
            sendString(ref sm, respstr);
        }
        
        while (shared.mode == State.LISTENING)
        {
            Thread.Sleep(50);
        }
    }

    /* Handle ship setup-- where should they go on the board? */
    private void runSetup(ref NetworkStream sm, int id)
    {
        if (shared.mode == State.SETUP)
        {
            if (shared.cstate[id] != State.READY)
            {
                respstr = "# Enter ship positions (? for random): ";
                sendString(ref sm, respstr);  
            }

            recvstr = receiveString(ref sm);
            
            if (recvstr == "?")
            {
                /* Add them randomly */
                shared.useRandomShips(id);
            }
            else
            {
                /* Add ships to the ship postition list */
                string[] shipstr = recvstr.Split(' ');
                foreach (string pos in shipstr)
                {
                    shared.ships[id].Add(pos);
                }
            }

            Console.Write($"** Game {shared.gameID}: P{clientID}: ");
            foreach (string pos in shared.ships[id])
            {
                Console.Write($"{pos} ");
            }
            Console.WriteLine();
            
            shared.cstate[id] = State.READY;      
                        
            if (shared.cstate[0] == State.READY && 
                shared.cstate[1] == State.READY)
            {
                shared.mode = State.PLAYING;
            }
            else
            {
                respstr = "! Waiting for other player to set up ships.\n";
                sendString(ref sm, respstr);
            }                        
        }

        /* If one client is done setting up but the other isn't, wait for them */
        while (shared.mode == State.SETUP)
        {
            Thread.Sleep(50);
        }
    }

    /* We're actually playing now */
    private void gameLoop(ref NetworkStream sm, int id)
    {
        if (shared.mode == State.PLAYING)
        {     
            /* If we're ready, have player 1 go first */               
            if (shared.cstate[0] == State.READY)
            {
                shared.cstate[0] = State.FIRING;
                shared.cstate[1] = State.WAITING;
            }

            /* The client that's waiting should do nothing */
            if (shared.cstate[id] == State.WAITING)
            {
                respstr = "! Waiting for opponent to fire.\n";
                sendString(ref sm, respstr);

                while (shared.cstate[id] == State.WAITING)
                {
                    Thread.Sleep(100);
                }

                /* But if they get hit, tell them */
                if (shared.hitFlag)
                {
                    respstr = $"! Ship hit at {shared.hitPos}.\n";
                    sendString(ref sm, respstr);
                    Thread.Sleep(10);
                }
                else if (!shared.hitFlag && shared.hitPos != "")
                {
                    respstr = $"! Opponent missed shot at {shared.hitPos}.\n";
                    sendString(ref sm, respstr);
                    Thread.Sleep(10);
                }
            }

            /* The client that's firing should send a position string */
            else if (shared.cstate[id] == State.FIRING)
            {
                shared.hitFlag = false;
                shared.hitPos = "";
                
                respstr = "# Enter position to fire upon (? for random): ";
                sendString(ref sm, respstr);

                recvstr = receiveString(ref sm);

                if (recvstr == "?")
                {
                    shared.hitPos = shared.getRandomPosition();
                }
                else
                {
                    shared.hitPos = recvstr;
                }

                /* If the position belongs to a corresponding ship... */
                shared.hitFlag = shared.ships[(id+1)%2].Any(
                    pos => pos == shared.hitPos);

                /* Either remove ship from set, or don't */
                if (shared.hitFlag)
                {
                    shared.ships[(id+1)%2].Remove(shared.hitPos);
                    respstr = $"! Successful hit at {shared.hitPos}.\n";
                    sendString(ref sm, respstr);
                }
                else
                {
                    respstr = $"! Shot missed at {shared.hitPos}.\n";
                    sendString(ref sm, respstr);
                }

                /* Swap! Now one is firing, and the other is waiting */
                shared.cstate[id] = State.WAITING;
                shared.cstate[(id+1)%2] = State.FIRING;
            }

            /* Is it over yet? */
            if (!shared.ships[id].Any())
            {
                shared.cstate[id] = State.LOSE;
                shared.cstate[(id+1)%2] = State.WIN;
                shared.mode = State.FINISHED;
                
                Console.WriteLine(
                    $"** Game {shared.gameID}: P{((id+1) % 2)+1} is the winner.");
            }         
        }

        /* If the game is finished, tell the winner they won, loser they lost, etc. */
        if (shared.mode == State.FINISHED)
        {
            if (shared.cstate[id] == State.WIN)
            {
                respstr = "! You are win. Press CTRL+C to exit.\n";
                sendString(ref sm, respstr);
                return;
            }
            else
            {
                respstr = "! You are lose. Press CTRL+C to exit.\n";
                sendString(ref sm, respstr);
                return;
            }               
        }
    }

    /* Method to chapperone this gameness and make sure the network works */
    private void playGame()
    {
        int id = clientID-1;   
             
        try
        {
            NetworkStream stream = clientSocket.GetStream();

            waitConnect(ref stream);
            runSetup(ref stream, id);
        
            while (shared.mode != State.FINISHED)
            {
                gameLoop(ref stream, id);
            }
        }
        catch (IOException)
        {
            Console.WriteLine(
                $"** Client {shared.gameID}:{clientID} terminated connection.");
                
            return;
        }
    }
}
