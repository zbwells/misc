using System;
using System.Text;

/* 
 * Helper enum type and shared info object that the server threads
 * can query to get info that relates to the handling of the game.
 * zbwells and crew
 */

public enum State 
{ 
    LISTENING, 
    WAITING, 
    SETUP, 
    READY, 
    PLAYING, 
    FIRING, 
    WIN, 
    LOSE, 
    FINISHED
}

class SharedInfo
{
    public State mode { get; set; }
    public State[] cstate { get; set; }
    public long gameID { get; set; }
    public bool hitFlag { get; set; }
    public string hitPos { get; set; }
    public HashSet<string>[] ships { get; set; }

    public SharedInfo(long g)
    {
        mode = State.LISTENING;
        cstate = new State[] { State.SETUP, State.SETUP };
        ships = new[] { new HashSet<string> {}, new HashSet<string> {} };
        gameID = g;
        hitFlag = false;
        hitPos = "";
    }

    /* Adds a random ship of a certain size to a certain player's board */
    private void addRandomShip(int size, int id)
    {
        Random rnd = new Random();
        bool horizontal = false;
        char alphapos;
        char digitpos;

        List<string> tmplist = new();

        /* Generate random positions based on orientation */
        /* 0 == horizontal, 1 == vertical */
        if (rnd.Next(0, 2) == 1)
        {
            alphapos = (char)(rnd.Next(0, 10)+65);
            digitpos = (char)(rnd.Next(0, size)+48);
        }
        else
        {
            alphapos = (char)(rnd.Next(0, size)+65);
            digitpos = (char)(rnd.Next(0, 10)+48);
            horizontal = true;
        }

        tmplist.Add($"{alphapos}{digitpos}");

        /* If the ship is to be inserted horizontally, iterate in that direction */
        if (horizontal)
        {
            for (int i = 1; i < size; i++)
            {
                alphapos = (char)((int)alphapos+1);
                tmplist.Add($"{alphapos}{digitpos}");
            }        
        }
        else
        {
            for (int i = 1; i < size; i++)
            {
                digitpos = (char)((int)digitpos+1);
                tmplist.Add($"{alphapos}{digitpos}"); 
            }
        }

        /* Check to see if there are duplicates in the list. */
        foreach (string pos in tmplist)
        {
            if (ships[id].Contains(pos))
            {
                addRandomShip(size, id);
                return;
            }
        }

        /* If control flow makes it to this point, safe to add points */
        foreach (string pos in tmplist)
        {
            ships[id].Add(pos);
        }    
    }

    /* Generate ships of varying sizes */
    public void useRandomShips(int id)
    {   
        addRandomShip(5, id);
        addRandomShip(4, id);
        addRandomShip(3, id);
        addRandomShip(3, id);
        addRandomShip(2, id);
    }

    /* Can totally fire in the same place more than once :) */
    public string getRandomPosition()
    {
        Random rnd = new Random();
        char alphapos = (char)(rnd.Next(0, 10)+65);
        char digitpos = (char)(rnd.Next(0, 10)+48);

        return $"{alphapos}{digitpos}";
    }
}
