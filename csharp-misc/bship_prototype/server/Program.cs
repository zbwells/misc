using System;
using System.Net;
using System.Threading;
using System.Net.Sockets;
using System.Linq;
using System.Text;

using static State;
using static SharedInfo;
using static HandleClient;

/* 
 * Class for generic server happenings relating to the battleship game.
 * Starts one game, and once clients connect, starts another and waits
 * for more clients.
 * Also handles when clients should move from waiting to setup mode.
 * zbwells and crew
 * Nov 2023
 */

class Server 
{
    public static void Main(string[] args)
    {
        int gameport = 2222;

        /* Handle arguments, set up port */
        if (args.Length < 1)
        {
            Console.WriteLine("** Using default port.");
        }
        else if (args.Length >= 1 && args[0][0] != '-')
        {
            gameport = int.Parse(args[0]);
        }
        else if (args[0] == "-h" || args[0] == "--help")
        {
            Console.WriteLine("Usage: bship_server <port>");
            return;
        }
        
        Console.WriteLine($"** Listening on port {gameport}");

        IPAddress ipAddress = IPAddress.Any;
        Serve(ipAddress, gameport, 0);
    }

    public static void Serve(IPAddress ipaddr, int port, long gameID)
    {
        int counter = 0;
        var ipEndPoint = new IPEndPoint(IPAddress.Any, port);
        TcpListener listener = new(ipEndPoint);
        TcpClient handler = new();

        /* After connecting, create new shared structure between two handlers */
        
        try
        {    
            listener.Start();
            SharedInfo sharedStruct = new(gameID);
            
            for (;;)
            {
                counter++;
                handler = listener.AcceptTcpClient();

                /* Create new client handler thread for client */
                Console.WriteLine($"** Game {gameID}: Client no. {counter} connected.");
                HandleClient client = new(handler, counter, ref sharedStruct);

                if (counter == 2)
                {
                    Console.WriteLine($"** Game {gameID}: Maximum clients reached.");
                    break;
                }
            }

            /* Make sure game goes after both clients connect */
            if (sharedStruct.mode == State.LISTENING)
            {
                sharedStruct.mode = State.SETUP;
            }
        }
        finally
        {   
            listener.Stop();
            Console.WriteLine($"** Game {gameID}: Starting game.");

            /* Do it again! billions of games running in parallel. */
            /* If you have infinite resources, that is. */
            Serve(ipaddr, port, gameID + 1);
        }    
    }
}


