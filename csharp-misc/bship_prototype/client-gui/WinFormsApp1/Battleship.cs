//CSC2230
//Battleship Group Assignment
//Early GUI for networked, multiplayer Battleship game.  At the moment only suports
//placing the player's boats in a simple manner.
//Zachary Hammonds et. al 

namespace Battleship
{
    public partial class Battleship : Form
    {
        //Battleship grid stored as an array of buttons.
        Button[,] PlayerArray = new Button[10, 10];
        Button[,] EnemyArray = new Button[10, 10];

        //Game state and number of ship placed.
        private bool GameStarted = false;
        private int ShipsPlaced = 0; 

        //Array of ships and ship sizes.
        private int[] ShipSizes = { 5, 4, 3, 3, 2 }; 
        private int CurrentShipIndex = 0;

        //Orientation of ships.
        private bool IsVertical = true;

        public Battleship()
        {
            InitializeComponent();
            BuildPlayerBoard();
            BuildOpponentBoard();

            //Keeping button state updated.
            foreach (Button button in PlayerArray)
            {
                button.Click += PlayerBoardButtonClick;
                button.Enabled = false;
            }
        }

        
        //What happens when clicking on a grid button.
        private void PlayerBoardButtonClick(object sender, EventArgs e)
        {
            if (!GameStarted)
            {
                MessageBox.Show("Please click 'Start' to begin the game.");
                return;
            }

            Button clickedButton = (Button)sender;
            int x = int.Parse(clickedButton.Name.Substring(6, 1));
            int y = int.Parse(clickedButton.Name.Substring(7, 1));

            PlaceShipOnPlayerBoard(x, y);


            // Check if the maximum number of ships is reached
            if (ShipsPlaced >= ShipSizes.Length)
            {
                foreach (Button button in PlayerArray)
                {
                    //Not placing any further ships.
                    button.Enabled = false; 
                }
            }
        }

        //Placing player ship on the game board.
        private void PlaceShipOnPlayerBoard(int x, int y)
        {
            int shipSize = ShipSizes[CurrentShipIndex];

            if (CanPlaceShip(x, y, shipSize, IsVertical))
            {
                for (int i = 0; i < shipSize; i++)
                {
                    if (IsVertical)
                    {
                        PlayerArray[x, y + i].BackColor = Color.Green;
                    }
                    else
                    {
                        PlayerArray[x + i, y].BackColor = Color.Green;
                    }
                }

                //Counting up index.
                ShipsPlaced++; 
                CurrentShipIndex++; 

                //Check if all ships are placed
                if (CurrentShipIndex >= ShipSizes.Length)
                {
                    MessageBox.Show("All ships placed!");
                    foreach (Button button in PlayerArray)
                    {
                        //Prevent more ships
                        button.Enabled = false; 
                    }
                }
            }
            else
            {
                MessageBox.Show("Cannot place the ship here. Choose a different location.");
            }
        }

        //Checking if ship can be placed, i.e. no overlapping ships or out of bounds.
        private bool CanPlaceShip(int x, int y, int size, bool isVertical)
        {
            if (isVertical && y + size <= 10)
            {
                for (int i = 0; i < size; i++)
                {
                    if (PlayerArray[x, y + i].BackColor == Color.Green)
                    {
                        return false;
                    }
                }
                return true;
            }
            else if (!isVertical && x + size <= 10)
            {
                for (int i = 0; i < size; i++)
                {
                    if (PlayerArray[x + i, y].BackColor == Color.Green)
                    {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }

        //Building player board where the player can place their ships.
        private void BuildPlayerBoard()
        {
            int Width = 40;
            int Height = 40;
            int x = 74;
            int y = 625;
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    if (j != 0)
                        x += 60;
                    Button b = new Button();
                    b.Size = new Size(Height, Width);
                    b.Location = new Point(x, y);
                    b.Text = "";
                    b.BackColor = System.Drawing.Color.LightGray;
                    b.FlatStyle = FlatStyle.Flat;
                    b.FlatAppearance.BorderColor = System.Drawing.Color.Blue;
                    b.FlatAppearance.BorderSize = 2;
                    b.UseVisualStyleBackColor = true;
                    b.TabStop = false;
                    b.Name = "button" + j.ToString() + i.ToString();
                    PlayerArray[j, i] = b;
                    this.Controls.Add(PlayerArray[j, i]);
                }
                y += 60;
                x = 74;
            }
        }
        //Building the opponent board, little more than placeholder atm.
        private void BuildOpponentBoard()
        {
            int Width = 40;
            int Height = 40;
            int x = 794;
            int y = 625;
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    if (j != 0)
                        x += 60;
                    Button b = new Button();
                    b.Size = new Size(Height, Width);
                    b.Location = new Point(x, y);
                    b.Text = "";
                    b.BackColor = System.Drawing.Color.LightGray;
                    b.FlatStyle = FlatStyle.Flat;
                    b.FlatAppearance.BorderColor = System.Drawing.Color.DarkRed;
                    b.FlatAppearance.BorderSize = 2;
                    b.UseVisualStyleBackColor = true;
                    b.TabStop = false;
                    b.Name = "buttonX" + j.ToString() + i.ToString();
                    EnemyArray[j, i] = b;
                    this.Controls.Add(EnemyArray[j, i]);
                }
                y += 60;
                x = 794;
            }
        }

        //Starting game.
        private void StartButton_Click(object sender, EventArgs e)
        {
            GameStarted = true;
            foreach (Button button in PlayerArray)
            {
                button.Enabled = true;
                StartButton.Enabled = false;
            }
        }

    }
}