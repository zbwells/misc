using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using static CSVData;
using static QueryUtils;

/*
 * Command line handler component of CSVCLI application.
 * Does things you expect a generic command-line app to do
 * zbwells, Nov 2023
 */

class CommandLine
{
    private CSVData data { get; set; } = null!;
    private FileStream fileHandle { get; set; } = null!;
    private StreamReader reader { get; set; } = null!;
    private bool closeCLI { get; set; }
    

    /* If no file provided, start CLI without a file loaded */
    public CommandLine()
    {
        Console.WriteLine("No file loaded currently.");
        Console.WriteLine("Enter .help to bring up a command list.");
        inputLoop();    
    }

    /* If file provided, start CLI and open file immediately */
    public CommandLine(string filename)
    {
        Console.WriteLine("Enter .help to bring up a command list.");
        LoadFile(filename);
        inputLoop();
    }
    
    /* Open CSV File and parse data into class */
    private void LoadFile(string filename)
    {
        if (!File.Exists(filename))
        {
            Console.WriteLine($"Could not find file {filename}.");
            return;
        }

        try 
        {
            fileHandle = new FileStream(filename, FileMode.Open);
            reader = new StreamReader(fileHandle);
            data = new CSVData(reader, ",", filename.Split(".")[0]);
            Console.WriteLine($"Loaded CSV file: \"{filename}\" as {data.tablename}.");
        }
        catch (Exception e)
        {
            Console.WriteLine($"Error: {e}");
            CLIQuit();
        }
    }

    /* Set a flag that tells the interpreter to quit next chance it gets */
    private void CLIQuit()
    {
        closeCLI = true;    
    }

    /* Parse the input from the command line and decide what to do with it */
    private void parseCommand(string cmd)
    {
        string[] words = cmd.Split(
            new[]{' '}, StringSplitOptions.RemoveEmptyEntries);

        if (words.Length < 1)
        {
            Console.WriteLine("Invalid command.");
            return;
        }
        
        if (fileHandle is null && 
            words[0] != ".load" && 
            words[0] != ".help" && 
            words[0] != ".quit")
        {
            Console.WriteLine("No file loaded.");
            return;
        }
        
        switch (words[0])
        {
            case ".schema":
                data.printHeadings();
                Console.WriteLine();
                break;
            case ".body":
                data.printAll();
                Console.WriteLine();
                break;
            case ".help":
            case "help":
                printHelp();
                break;
            case ".column":
                try
                {
                    data.printColumn(words[1]);
                    Console.WriteLine();
                }
                catch (IndexOutOfRangeException)
                {
                    Console.WriteLine("Must provide column name.");
                }
                break;
            case ".load":
                try
                {
                    LoadFile(words[1]);
                }
                catch (IndexOutOfRangeException)
                {
                    Console.WriteLine("Must provide filename.");
                }
                break;
            case "quit":
            case ".quit":
                CLIQuit();
                break;
            case "select":
            case "SELECT":
                QueryUtils.runQuery(data, words);
                Console.WriteLine();
                break;
            default:
                Console.WriteLine("Invalid command.");
                break;
        }
    }

    /* Print out semi-comprehensive help page that might be helpful */
    private void printHelp()
    {
        Console.WriteLine("Non-query commands: ");
        Console.WriteLine("\t.load <file>\tLoad CSV file.");
        Console.WriteLine("\t.column <name>\tShow a specific column.");
        Console.WriteLine("\t.schema\t\tShow the available columns of the file.");
        Console.WriteLine("\t.body\t\tShow all rows and columns from file.");
        Console.WriteLine("\t.help\t\tShow this help page.");
        Console.WriteLine("\t.quit\t\tClose file and quit.\n");   
        Console.WriteLine();
        Console.WriteLine("Syntax differs somewhat from real SQl.");
        Console.WriteLine("Example queries:");
        Console.WriteLine("  SELECT *;\n\tGets all rows and columns.");
        Console.WriteLine(
        "  SELECT * WHERE age < 25;\n\tIncludes only rows where \'age\' is less than 25.");
        Console.WriteLine(
        "  SELECT name, age WHERE name = \"Bobby\";\n\tIncludes name and age where \'name\' is Bobby.");
         
    }

    /* Ask for input, and after it's parsed, ask again */
    private void inputLoop()
    {
        for (;;)
        {
            Console.Write("> ");
            string? inp = Console.ReadLine();
            
            if (inp is not null)
            {
                parseCommand(inp);
            }
            else
            {
                return;    
            }

            if (closeCLI == true)
            {
                return;
            }
        }
    }
}
