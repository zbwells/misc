using System;
using System.Linq;
using System.Collections.Generic;
using static CSVData;

/* 
 * File that holds the static class (library) QueryUtils,
 * which relies on CSVData... etc
 * This holds all of the operations required to actually 
 * query the CSV file.
 * Also ended up as a nightmare and an unreadable mess. Read at your peril.
 * zbwells Nov, 2023
 */
 
static class QueryUtils
{
    /* little helper function to remove uwnanted characters */
    static private string removeSemicolon(string str)
    {
        return new String(
            str.Where(ch => Char.IsLetterOrDigit(ch)).ToArray());    
    }

    /* Filters out rows of a CSV object based on some given data */
    static private CSVData filterRows(
        CSVData csv, string op, string colname, string checkname)
    {
        List<int> includeRows = new();
        CSVData tempcopy = csv.emptyCopyCSV();

        for (int i = 0; i < csv[colname].entries.Count; i++)
        {
            dynamic datapoint = csv[colname].entries[i]!;
            if (op == "=")
            {
                if (datapoint.ToString() == checkname)
                {
                    includeRows.Add(i);
                }
            }
            else if (op == "<")
            {
                if (datapoint.datum < Convert.ToDouble(checkname))
                {
                    includeRows.Add(i);
                }
            }
            else if (op == ">")
            {
                if (datapoint.datum > Convert.ToDouble(checkname))
                {
                    includeRows.Add(i);
                }
            }
            else if (op == "<=")
            {
                if (datapoint.datum <= Convert.ToDouble(checkname))
                {
                    includeRows.Add(i);
                }
            }
            else if (op == ">=")
            {
                if (datapoint.datum >= Convert.ToDouble(checkname))
                {
                    includeRows.Add(i);
                }
            }
            else if (op == "!=")
            {
                if (datapoint.ToString() != checkname)
                {
                    includeRows.Add(i);
                }
            }                                                    
        }

        /* Loop through the list of whitelisted rows, and add them to the new obj  */
        foreach (CSVColumn col in csv.columns)
        {
            for (int i = 0; i < csv[col.heading].entries.Count; i++)
            {
                foreach (int rownum in includeRows)
                {
                    if (rownum == i)
                    {                                    
                        tempcopy[col.heading].entries.Add(
                            csv[col.heading].entries[i]);
                    }
                }
            }
        }

        return tempcopy;          
    }

    /* This gets called when you actually want to run a query, and it parses the query */
    static public void runQuery(CSVData csv, string[] query)
    { 
        bool whereUsed = false;
        string comparisonOp = "";
        string colname = "";

        List<string> columnsToQuery = new List<string> {};
        CSVData tempdata = new CSVData();
        
        if (query.Length < 2)
        {
            Console.WriteLine("Err: Must specify what columns to query.");
            return;
        }

        /* Loop through each word in the query. aka great value tokenizier */
        foreach (string word in query)
        {
            if (!whereUsed && word[0] == '*')
            {
                columnsToQuery = (from col in csv.columns
                                 select col.heading).ToList();
            }
            else if (!whereUsed && word.ToUpper() != "SELECT")
            {
                colname = removeSemicolon(word);
                    
                columnsToQuery.Add(colname);
            }

            if (!whereUsed && word.ToUpper() == "WHERE")
            {
                colname = "";
                whereUsed = true;
            }

            /* fun */
            if (whereUsed && word.ToUpper() != "WHERE") 
            {                   
                if (comparisonOp != "")
                {
                    string checkname = removeSemicolon(word);
                    csv = filterRows(csv, comparisonOp, colname, checkname);    
                }
                else if (comparisonOp == "" && colname == "")
                {
                    colname = removeSemicolon(word);
                }
                else if (comparisonOp == "")
                {
                    comparisonOp = word;
                }

                if (comparisonOp == "" && word[word.Length-1] == ';')
                {
                    Console.WriteLine("Err: Must specify data for matching.");
                    break;
                }
            }
            
            if (word[word.Length-1] == ';')
            {
                tempdata = csv.getFields(columnsToQuery.ToArray());
                tempdata.printAll();
                break;
            }  
        }
    }
}
    
