using System;
using static CommandLine;

/* 
 * Driver class for CSVCLI application.
 * All the fun stuff is in the other files, this one just 
 * does some basic preparation and prints a help page if you use -h.
 * zbwells, Nov 2023
 */

class Runner
{
    public static void Main(string[] args)
    {
        string filename = "";

        /* If no arguments provided, start without pre-loading file. */
        if (args.Length < 1)
        {
            new CommandLine();     
        }
        else
        {
            /* Print help page if asked for. */
            if (args[0] == "--help" || args[0] == "-h")
            {
                Console.WriteLine("Usage: csvcli <optional filename>");
                Console.WriteLine("Type .help in CLI interface for more info.");
                return;
            }
            
            filename = args[0];
            new CommandLine(filename);
        }

        /* After control is returned from CommandLine class */
        Console.WriteLine("Exiting.");
    }
}
