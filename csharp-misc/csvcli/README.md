### CSVCLI

Obviously, I didn't end up being able to implement all of the features I wanted,
but I got close enough for an academic project thingy. I deem the querying langauge
"NQSQL", which stands for "Not Quite SQL", because it really doesn't act like SQL at all.

I might revisit this and make an actual SQL implementation, which would take much
more time and effort and I'd probably borrow Microsoft SQL Server's SQL parser library.

I'd probably also borrow somebody's CSV parser implementation, or make my own better.
If the CSV file is too large, or uses something besides commas, my implementation currently
can't deal with it, so that's somewhat problematic.

Anyways, here's some sample usage:

```
No file loaded currently.
Enter .help to bring up a command list.
> .load sample.csv
Loaded CSV file: "sample.csv" as sample.
> .schema
firstname, lastname, age, 

> .body
firstname, lastname, age, 
bob, builder, 88, 
tim, terrible, 44, 
malcom, x, 150, 
ralph, nader, 88, 
megaman, x, 2000, 

> SELECT lastname, firstname WHERE age > 50;
lastname, firstname, 
builder, bob, 
x, malcom, 
nader, ralph, 
x, megaman, 

> SELECT age WHERE lastname = "x";
age, 
150, 
2000, 

> SELECT *;
firstname, lastname, age, 
bob, builder, 88, 
tim, terrible, 44, 
malcom, x, 150, 
ralph, nader, 88, 
megaman, x, 2000, 

> 
```
There's a huge list of unimplemented things, but most notably,
the ability to filter out multiple things at the same time,
and the ability to order rows based on some value (asc or desc)
are missing.

zbwells
