using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;

/*
 * File for custom data structure CSVData:
 * CSVEntry and CSVColumn could probably just be replaced by a Dictionary with ArrayLists
 * I wanted to experiment with some of C#'s features more, so custom data structure.
 * Kind of hacked up, if I had more time I would absolutely rewrite this section.
 * Too complicated, hard to read, etc.
 * zbwells, Nov 2023
 */

 /* Also, could've just used somebody elses' battle-proven CSV library--
  * but I wanted to try a custom implementation, like I mentioned. */
 
/* The most basic use of generics possible, but it's here */
class CSVEntry<T>
{
    public string type { get; private set; }
    public T datum { get; private set; }

    public CSVEntry(T d, string t)
    {
        type = t;
        datum = d;
    }

    public override string? ToString()
    {   
        if ( datum is not null)
        {
            return datum.ToString();
        }
        else
        {
            return "";
        }
    }
}

/* A class for a column of the CSV file */
class CSVColumn
{   
    public string heading { get; set; }
    public ArrayList entries { get; set; }

    public CSVColumn(string name)
    {
        heading = name;
        entries = new ArrayList();
    } 
}

/* Class to generate object which will hold all the CSV info in a useful manner */
class CSVData
{
    public List<CSVColumn> columns { get; set; }
    public string tablename { get; private set; }
    public string delim { get; private set; }
    public int rowcount { get; set; }
    private int colcount { get; set; }

    /* Empty constructor overload-- make empty CSV object to modify later */
    public CSVData()
    {
        columns = new List<CSVColumn> {};
        delim = ",";
        tablename = "temp";
    }

    /* Generate CSV object, but with data from a file */
    public CSVData(StreamReader file, string d, string name)
    {
        columns = new List<CSVColumn> {};
        delim = d;
        tablename = name;

        populateList(file);
    }
    
    /* Define indexer for CSVData */
    public CSVColumn this[string name]
    {
        get 
        {
            try
            {
                var query = from col in columns 
                            where col.heading == name
                            select col;

                return query.First();
            }
            catch (InvalidOperationException)
            {
                Console.WriteLine($"Err: Column {name} not found.");
                return new CSVColumn("empty");
            }              
        } 
    }

    /* Make an empty copy (just the headings) from another CSV object */
    public CSVData emptyCopyCSV()
    {
        CSVData temp = new();
        foreach (CSVColumn col in columns)
        {
            temp.columns.Add(new CSVColumn(col.heading));
            temp.colcount++;
        }

        return temp;    
    }

    /* Actually add the stuff from the file to the CSVColumns and stuff */
    private void populateList(StreamReader reader)
    {
        colcount = -1;
        rowcount = -1;
        
        while (!reader.EndOfStream)
        {
            rowcount++;
            
            string? line = reader.ReadLine();
            int intentry;
            double doubleentry;

            if (line is null)
            {
                Console.WriteLine("unknown error :)");
                return;
            }
            
            foreach (var word in line.Split(
                new[]{',', ' '}, StringSplitOptions.RemoveEmptyEntries))
            {
                colcount++;
                   
                if (rowcount == 0)
                {
                    columns.Add(new CSVColumn(word));
                    continue;    
                }

                /* Seems like redundancy, but entryobj is actually three unrelated vars */
                /* Could probably fnagle this somehow betterer but for now it works */
                if (int.TryParse(word, out intentry))
                {
                    CSVEntry<int> entryobj = new CSVEntry<int>(intentry, "integer");
                    columns.ElementAt(colcount).entries.Add(entryobj);
                }
                else if (double.TryParse(word, out doubleentry))
                {
                    CSVEntry<double> entryobj = new CSVEntry<double>(doubleentry, "double");
                    columns.ElementAt(colcount).entries.Add(entryobj);
                }
                else
                {
                    CSVEntry<string> entryobj = new CSVEntry<string>(word, "string");
                    columns.ElementAt(colcount).entries.Add(entryobj);
                }                 
            }

            colcount = -1;
        }  
    }

    /* Print out a single column of the CSV object */
    public void printColumn(string heading)
    {
        foreach (dynamic entry in this[heading].entries)
        {
            Console.WriteLine(entry.datum);
        }
    }

    /* Print all the columns */    
    public void printAll()
    {
        printHeadings();
        
        for (int i = 0; i < columns.ElementAt(0).entries.Count; i++)
        {
            foreach (dynamic col in columns)
            {
                Console.Write($"{col.entries[i].datum}, ");
            }

            Console.WriteLine();
        }
    }

    /* Get specific fields (when querying, this is used) */
    public CSVData getFields(string[] fields)
    {
        CSVData temp = new CSVData();

        foreach (string str in fields)
        {
            foreach (dynamic col in columns)
            {
                if (col.heading == str)
                {
                    temp.colcount++;
                    temp.columns.Add(col);
                }
            }
        }

        temp.rowcount = rowcount;
        return temp;
    }

    /* Print out specific fields from the CSV object */
    public void printFields(string[] fields)
    {
        foreach (string str in fields)
        {
            Console.Write($"{str}, ");
        }

        Console.WriteLine();
        
        for (int i = 0; i < rowcount; i++)
        {
            foreach (dynamic str in fields)
            {
                Console.Write($"{this[str].entries[i].datum}, ");
            }

            Console.WriteLine();
        }

    }

    /* Print out all the headings from the CSV file */
    public void printHeadings()
    {
        foreach (var col in columns)
        {
            Console.Write($"{col.heading}, ");
        }

        Console.WriteLine();
    }
}
