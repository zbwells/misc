using System;
using System.Collections.Generic;
using System.Linq;

// prefix notation calculator (+ 15 10) lisp style, but without parens

class Program
{
    private static void Main()
    {
        var operands = new Stack<int> {};
        string input;

        while ((input = Console.ReadLine()) != null)
        {
            foreach (string word in input.Split())
            {
                ParseInp(word, operands);
            }
        }
    }
    
    private static void ParseInp(string word, Stack<int> ops)
    { 
        int num = 0;
        bool isnum = int.TryParse(word, out num);

        try
        {
            switch (word)
            {
                case "p":
                    Console.WriteLine(ops.Peek()); 
                    break;
                case "c":
                    ops.Clear();
                    break;
                case "q":
                    Environment.Exit(0);
                    break;
                case "+": 
                    ops.Push(ops.Pop() + ops.Pop());
                    break;
                case "-":
                    ops.Push(-ops.Pop() + ops.Pop());
                    break;
                case "*":
                    ops.Push(ops.Pop() * ops.Pop());
                    break;
                case "/":
                    ops.Push(ops.Pop() / ops.Pop());
                    break;
                default:
                    if (isnum) ops.Push(num);   
                    break;
            }
        }
        catch (InvalidOperationException)
        {
            Console.WriteLine("postfix: stack empty");
        }
    }
}

