using System;
using System.Collections.Generic;
using System.Linq;

// prefix notation calculator (+ 15 10) lisp style, but without parens

class Program
{
    private static void Main()
    {
        var numbers = new Stack<string> {};
        var operands = new Stack<int> {};

        string input = Console.ReadLine();

        foreach (string word in input.Split())
        {
            numbers.Push(word);
        }

        Calc(numbers, operands);
        Console.WriteLine(numbers.Pop());
    }

    private static string Calc(Stack<string> nums, Stack<int> ops)
    {
        string dat = "";
        int num = 0;
        
        for (;;)
        {   
            dat = nums.Pop();
            int.TryParse(dat, out num);
            
            switch (dat)
            {
                case "+":
                    nums.Push($"{ops.Pop() + ops.Pop()}");
                    break;
                case "-":
                    nums.Push($"{ops.Pop() - ops.Pop()}");
                    break;
                case "*":
                    nums.Push($"{ops.Pop() * ops.Pop()}");
                    break;
                case "/":
                    nums.Push($"{ops.Pop() / ops.Pop()}");
                    break;
                default:
                    ops.Push(num);
                    break;
            }

            if (int.TryParse(nums.Peek(), out num) && nums.Count == 1)
            {
                break;
            }
        }

        return dat;
    }
}

