# A clone of the wc program from GNU Coreutils.
# Jan 9 2022
# zbw <zbw9uc@uvawise.edu>

## to read stdin and command line arguments
import sys
from args import args_data	
from data import file_data
from help import print_help_message	

def count_files(argv):
		count = 0
		for word in argv[1:]:
			if word[0] != '-':
				count += 1
		
		return count	

def main(argc, argv):
	filecount = count_files(argv)
	
	# Make all the argument flags into a dictionary
	args = args_data(argv)
	
	# Print help if called
	if args.help == True:
		print_help_message()
		return
		
	if args.file_usage == True:	
		files = []
		for num in range(filecount+1):
			files.append(file_data())
		
		# Generate file data for each of the files supplied as arguments
		temp_count = 0
		for word in argv[1:]:
			if word[0] != '-':
				files[temp_count].filename = word
				files[temp_count].count_from_file(word, args)
				temp_count += 1
		
		
		# If there is more than one file
		if filecount > 1:
			total = file_data()
			
			for num in range(filecount):
				total.filename = "total"
				total.lines += files[num].lines
				total.words += files[num].words
				total.chars += files[num].chars
			
			total.print_total(args)
		
	else:
		data = file_data()
		# Get data from stdin if there are no filenames
		try:
			data.count_from_stdin(args)
		except KeyboardInterrupt:
			print()
			quit()

			
## Run main with command line arguments included as arguments to main(), C style
# But only if this file is the main entry point

if __name__ == "__main__":
	main(len(sys.argv), sys.argv)
	
	