# A clone of the wc program from GNU Coreutils. Redesigned with classes.
# Jan 9 2022
# zbw <zbw9uc@uvawise.edu>

from sys import stdin
from sys import stderr

### Class to hold file data and methods to operate on that 
class file_data:
	
	def __init__(self):
		self.lines = 0
		self.words = 0
		self.chars = 0
		self.filename = ""
	
	## Populates a dictionary with actual data about a file
	def populate_data_dict(self, file):
		file_str = ""
		temp = 0
		
		for line in file:
			for ch in line:
				if ch == '\n':
					self.lines += 1
					temp = 0
				elif ch == ' ' or ch == '\t':
					temp = 0
				elif temp == 0:
					temp = 1
					self.words += 1
				
				self.chars += 1
	
	## Prints file scores in a file opening context
	def print_total(self, args):
		if args.words == False and args.lines == False and args.chars == False:
			print( "%4u %4u %4u " % ( self.lines, 
				self.words, self.chars ), end='' );
		else:
			if args.words != False:
				print( "%4u " % self.words, end='' )
			if args.lines != False:
				print( "%4u " % self.lines, end='' )
			if args.chars != False:
				print( "%4u " % self.chars, end='')
			
		print( "%4s" % self.filename ) 
	
	## Passes standard input to the dictionary creation function and prints out the data
	def count_from_stdin(self, args):
		self.populate_data_dict(stdin);
		if args.words == False and args.lines == False and args.chars == False:
			print( "%7u %7u %7u " % ( self.lines, 
				self.words, self.chars ), end='' );
		else:
			if args.words != False:
				print( "%7u " % self.words, end='' )
			if args.lines != False:
				print( "%7u " % self.lines, end='' )
			if args.chars != False:
				print( "%7u " % self.chars, end='' )
			
		print() 
	
	## Opens a file and passes it to the dictionary creation function and prints out the data
	def count_from_file(self, filename, args):
		try:
			file = open(filename, "r")
		except FileNotFoundError:
			print("zwc: %s: No such file or directory" % filename, file=stderr)
			return
		except IsADirectoryError:
			print("zwc: %s: Is a directory" % filename, file=stderr)
			return
		except PermissionError:
			print("zwc: %s: Permission denied" % filename, file=stderr)
			return
		except:
			print("zwc: %s: Error opening file" % filename, file=stderr)
			return
		
		# GNU wc pretends to count the directory's characters and such anyways.
		# which means that so will I, even after error checking.
		
		self.populate_data_dict(file)
		self.print_total(args)
		file.close();
	