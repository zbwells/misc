# A clone of the wc program from GNU Coreutils. 
# Jan 11 2022
# zbw <zbw9uc@uvawise.edu>

# Argument handling class

class args_data:
	
	def __init__(self, argv):
		self.file_usage = False
		self.help = False
		self.words = False
		self.lines = False
		self.chars = False
		self.handle_arguments(argv)
		
		
	## Make a dictionary and populate it with argument handling data
	def handle_arguments(self, argv):
		for word in argv[1:]:
			if word == "-h" or word == "--help":
				self.help = True
				break
			if word[0] != '-':
				self.file_usage = True
			if word == "-w" or word == "--words":
				self.words = True
			if word == "-l" or word == "--lines":
				self.lines = True
			if word == "-c" or word == "--chars":
				self.chars = True


			