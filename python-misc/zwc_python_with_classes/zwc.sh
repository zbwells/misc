## This is a wrapper for the wc clone, zwc
## This just calls the program, which locally lives in "src"

/usr/bin/env python3 ./src $@

## Running the python interpreter and providing the path to the directory containing the source files by any other method should also work.

# To clarify: $@ is all of the arguments provided on the command line.