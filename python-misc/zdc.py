## reverse polish calc in python because iam bored
## I have a feeling I did this the hardest way possible

## Use at your own risk, assume it works like GNU dc, but not always :)
## TODO: redo this but with a proper stack instead of a sack and implement more of the usual features
## (sack is made-up, it's just a list; there is no such data structure called sack that I am aware of)

# Otherwise comes with zero documentation.

## Z. Wells, Aug 5

from sys import stdin, stderr
from operator import add, sub, mul, floordiv, mod

# Turn input into a form that can easily be used to calculate numbers
def stackify():
	sack = []
	line = []
	
	while 'q' not in line:
		line = input().split(' ')
			
		sack += line
		
		if 'p' in line:
			sack.remove('p')
			if sack == []:
				print("dc: stack empty", file=stderr)
			else:
				sack = operate(sack)
				result = sack[-1:][0]
				print(result)
		
		if 'c' in line:
			sack.remove('c')
			sack = []
		
		if 'f' in line:
			sack.remove('f')
			sack = operate(sack)
			print(*sack)
		
		if 'n' in line:
			sack.remove('n')
			sack = operate(sack)
			print(sack[0])

# Read the list (or the 'sack') and calculate numbers; reverse polish notation
def operate(sack):
	operator_list = "+-*/%"
	sack = sack[:]
	
	def edit_sack(math_op):
		temp_calc = math_op(int(sack[i-2]), int(sack[i-1]))
		sack[i-2] = str(temp_calc)
		del sack[i-1:i+1]
	
	arentdigits = lambda : not sack[i-1].isdigit() or not sack[i-2].isdigit()
	ops_in_sack = lambda : {ch in operator_list for ch in sack}
	
	while True in ops_in_sack():
		for i in range(len(sack)-1, 0, -1):
			if sack[i] == '+':
				if arentdigits():
					continue 
				else:
					edit_sack(add)
					break
					
			elif sack[i] == '-':
				if arentdigits():
					continue 
				else:
					edit_sack(sub)
					break
			
			elif sack[i] == '*':
				if arentdigits():
					continue 
				else:
					edit_sack(mul)
					break
			
			elif sack[i] == '/':
				if arentdigits():
					continue 
				else:
					edit_sack(floordiv)
					break
			
			elif sack[i] == '%':
				if arentdigits():
					continue 
				else:
					edit_sack(mod)
					break

			elif not sack[i].isdigit():
				del sack[i]
				
	return sack
	
def main():
	try:
		stackify()
	except EOFError:
		exit(0)
	except KeyboardInterrupt:
		print()
		exit(0)
	
if __name__ == "__main__":
	main()
	