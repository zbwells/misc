# A clone of the wc program from GNU Coreutils.
# Jan 10 2022
# zbw <zbw9uc@uvawise.edu>

### Help file.

def print_help_message():
	print("Usage: zwc [OPTION]... [FILE]...\n\nPrint lines, words, and character counts for each FILE, and a total line if more than one FILE is specified. This order is maintained regardless of the order in which options are provided. A word is a non-zero-length sequence of printable characters delimited by whitespace. \n\nIf no FILE is specified, input is taken from stdin.\n\nOptions: \n\t -c,  --chars \t\t\tprint the character count\n\t -l, --lines \t\t\tprint the line count\n\t -w, --words \t\t\tprint the word count\n\t -h, --help \t\t\tprint this help message\n\nzwc is a GNU Coreutils wc clone program intended for student/study/practice usage.\nAuthor: Z. Wells, Spring 2022 UVA Wise CSC Tutor\nEmail: zbw9uc@uvawise.edu\n", end='')
	

# This implementation of a help page is taken directly from the C version of my wc clone.
# Leaves a lot to be desired, but it'll do
# zbw