# A clone of the wc program from GNU Coreutils. 
# Jan 11 2022
# zbw <zbw9uc@uvawise.edu>

# Argument handling routines and stuff

# Loop through the arguments to get how many files there are
def count_files(argv):
	count = 0
	for word in argv[1:]:
		if word[0] != '-':
			count += 1
	
	return count


## Make a dictionary and populate it with argument handling data
def handle_arguments(argv):
	args_dict = {
		"file_usage" : False,
		"help" : False,
		"words" : False,
		"lines" : False,
		"chars" : False
	}
	
	for word in argv[1:]:
		if word == "-h" or word == "--help":
			args_dict["help"] = True
			break
		if word[0] != '-':
			args_dict["file_usage"] = True
		if word == "-w" or word == "--words":
			args_dict["words"] = True
		if word == "-l" or word == "--lines":
			args_dict["lines"] = True
		if word == "-c" or word == "--chars":
			args_dict["chars"] = True
		
	return args_dict


			