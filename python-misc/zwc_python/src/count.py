# A clone of the wc program from GNU Coreutils.
# Jan 9 2022
# zbw <zbw9uc@uvawise.edu>

## to read stdin and command line arguments
from sys import stdin
from sys import stderr
from args import count_files 
from args import handle_arguments
from help import print_help_message

## Creates a dictionary to hold data about files
def create_data_dict():
	file_dict = { 
		"lines" : 0, 
		"words" : 0, 
		"chars" : 0, 
		"filename" : "" 
	}
	
	return file_dict


## Populates a dictionary with actual data about a file
def populate_data_dict(file, file_dict):
	file_str = ""
	temp = 0
	
	for line in file:
		for ch in line:
			if ch == '\n':
				file_dict["lines"] += 1
				temp = 0
			elif ch == ' ' or ch == '\t':
				temp = 0
			elif temp == 0:
				temp = 1
				file_dict["words"] += 1
			
			file_dict["chars"] += 1
			
	
	
def print_total(file_dict, args):
	if args["words"] == False and args["lines"] == False and args["chars"] == False:
		print( "%4u %4u %4u " % ( file_dict["lines"], 
			file_dict["words"], file_dict["chars"] ), end='' );
	else:
		if args["words"] != False:
			print( "%4u " % file_dict["words"], end='' )
		if args["lines"] != False:
			print( "%4u " % file_dict["lines"], end='' )
		if args["chars"] != False:
			print( "%4u " % file_dict["chars"], end='')
		
	print( "%4s" % file_dict["filename"] ) 
	
	
## Passes standard input to the dictionary creation function and prints out the data
def count_from_stdin(file_dict, args):
	populate_data_dict(stdin, file_dict);
	if args["words"] == False and args["lines"] == False and args["chars"] == False:
		print( "%7u %7u %7u " % ( file_dict["lines"], 
			file_dict["words"], file_dict["chars"] ), end='' );
	else:
		if args["words"] != False:
			print( "%7u " % file_dict["words"], end='' )
		if args["lines"] != False:
			print( "%7u " % file_dict["lines"], end='' )
		if args["chars"] != False:
			print( "%7u " % file_dict["chars"], end='' )
		
	print() 
	
	
## Opens a file and passes it to the dictionary creation function and prints out the data
def count_from_file(filename, file_dict, args_dict):
	try:
		file = open(filename, "r")
	except FileNotFoundError:
		print("zwc: %s: No such file or directory" % filename, file=stderr)
		return
	except IsADirectoryError:
		print("zwc: %s: Is a directory" % filename, file=stderr)
		return
	except PermissionError:
		print("zwc: %s: Permission denied" % filename, file=stderr)
		return
	except:
		print("zwc: %s: Error opening file" % filename, file=stderr)
		return
	
	# GNU wc pretends to count the directory's characters and such anyways.
	# which means that so will I, even after error checking.
	
	populate_data_dict(file, file_dict)
	print_total(file_dict, args_dict)
	file.close();
		
					

def main(argc, argv):
	filecount = count_files(argv)
	
	# Make all the argument flags into a dictionary
	args_dict = handle_arguments(argv)
	
	# Print help if called
	if args_dict["help"] == True:
		print_help_message()
		return
		
		
	if args_dict["file_usage"] == True:	
		file_data = []
		for num in range(filecount+1):
			file_data.append(create_data_dict())
		
		# Generate file data for each of the files supplied as arguments
		temp_count = 0
		for word in argv[1:]:
			if word[0] != '-':
				file_data[temp_count]["filename"] = word
				count_from_file(word, file_data[temp_count], args_dict)
				temp_count += 1
		
		
		# If there is more than one file
		if filecount > 1:
			total = create_data_dict()
			
			for num in range(filecount):
				total["filename"] = "total"
				total["lines"] += file_data[num]["lines"]
				total["words"] += file_data[num]["words"]
				total["chars"] += file_data[num]["chars"]
			
			print_total(total, args_dict)
		
	else:
		file_data = create_data_dict()
		# Get data from stdin if there are no filenames
		try:
			count_from_stdin(file_data, args_dict)
		except KeyboardInterrupt:
			print()
			quit()
			
## Main gets run in __main__.py
	
	