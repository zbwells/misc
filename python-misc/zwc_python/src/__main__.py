import sys
from count import main

## Run main with command line arguments included as arguments to main(), C style
# But only if this file is the main entry point

if __name__ == "__main__":
	main(len(sys.argv), sys.argv)