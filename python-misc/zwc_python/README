Greetings! This is a very minimal clone of the 'wc' utility. I have also a C version. Most of this readme is borrowed from there. Once more, the tool holds little value as an actual production application-- it is merely meant to be an example of a hypothetical distributable program, for 'learning purposes'.

I have prepared a shell script to run the program that will probably work on most Unix-like operating systems, since most happen to use Bash and those that don't and are from the 21st century likely have some compatibility.

Either way, if you wanted to run it from the command line without using the shell-script, do:

$ python3 ./src <arguments>

If you are already familiar with the wc utility, zwc is no different. A quick tutorial of its usage follows. (Borrowed from the C version's README.)

$ ./zwc.sh

Because it is accepting input from standard input (stdin, which is where your keyboard input goes) and waiting for end-of-file, you can type whatever you like, and nothing will happen. To induce EOF (end-of-file), on a standard US keyboard you can hit Ctrl-D and the program will tell you the line, word, and character counts of what you have just entered. If you want to use this in a way more intended, try:

$ cat test_file | ./zwc.sh 
      6      21     135

If your shell doesn't support pipes, skip down to the next example usage.

"cat test_file" outputs the data contained in "test_file" which is included with the source, which should be a couple of lines of nonsense. "|" is a pipe-- which takes the output from the first command, and stuffs it into standard input for the second command, as if you had entered it with the keyboard. This allows zwc to act on that input, and get the word count for the output of "cat test_file".
      
Notice the formatting. You can also achieve the same results but in a different way by running:

$ ./zwc.sh test_file
   6   21  135 test_file
   
The formatting is different; zwc acts this way purely because it is a simple clone of GNU wc.

Support is also given for counting more than one file and comparing their word, line, and character counts. More information on usage can be provided by running:

$ ./zwc.sh --help

Thanks.

Copyright © 2022 Zander Wells <zbw9uc@uvawise.edu>
This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See the COPYING file for more details.
