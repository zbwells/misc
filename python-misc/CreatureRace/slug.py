## Slug object, which handles the Slug's movements during the race.
## Jul 17 2022
## zbwells <zbwells@protonmail.com>

'''Python implementation of Benjamin Vipperman's CreatureRace program, written for Dr. Jacob Somervelle's CSC 1180 (Programming in C++) course at The University of Virginia's College at Wise.'''

from random import randint
from creature import Creature

class Slug(Creature):
   def __init__(self):
      super().__init__()
      self.winMsg = "Slug WINS!! WHOO HOO!"
      self.symbol = "S"
   
   def move(self):
      perc = randint(0, 100)
      
      if perc <= 20:
         # 20% Chance to move left 3
         self.position = self.position - 3
      elif perc <= 50:
         # 30% chance to move right 3
         self.position = self.position + 3
      elif perc <= 65:
         # 15% chance to move left 1
         self.position = self.position - 1
      else:
         # 35% chance to move right 1
         self.position = self.position + 1
      
      # If the slug retreats too far, put them back at the starting line
      if self.position < 1:
         self.position = 1
      