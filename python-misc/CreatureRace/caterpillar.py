## Caterpillar object, which handles the Caterpillar's movements during the race.
## Jul 17 2022
## zbwells <zbwells@protonmail.com>

'''Python implementation of Benjamin Vipperman's CreatureRace program, written for Dr. Jacob Somervelle's CSC 1180 (Programming in C++) course at The University of Virginia's College at Wise.'''

from random import randint
from creature import Creature

class Caterpillar(Creature):
   def __init__(self):
      super().__init__()
      self.winMsg = "Caterpiller wins, yay"
      self.symbol = "C"
   
   def move(self):
      perc = randint(0, 100)
      
      if perc <= 10:
         # 10% chance to not move at all
         self.position = self.position
      elif perc <= 30:
         # 20% chance to move right 6
         self.position = self.position + 6
      elif perc <= 50:
         # 20% chance to move left 6
         self.position = self.position - 6
      elif perc <= 80:
         # 30% chance to move right 2
         self.position = self.position + 2
      else:
         # 20% chance to move left 1
         self.position = self.position - 1
      
      # If caterpillar retreats too far, put him back at the starting line
      if self.position < 1:
         self.position = 1
      

