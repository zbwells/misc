## Puts all the given race contestants into a race against each other, to be print to the screen.
## Jul 17 2022
## zbwells <zbwells@protonmail.com>

'''Python implementation of Benjamin Vipperman's CreatureRace program, written for Dr. Jacob Somervelle's CSC 1180 (Programming in C++) course at The University of Virginia's College at Wise.'''

from time import sleep
from caterpillar import Caterpillar
from slug import Slug

class Race:
   def __init__(self, length, speed):
      self.racelen = length
      self.speed = speed
      self.cater = Caterpillar()
      self.slug = Slug()
   
   def go(self):
      while self.slug.position < self.racelen and self.cater.position < self.racelen:
         self.slug.move()
         self.cater.move()
         self.printRace()
         sleep(self.speed)
      
      if self.slug.position >= self.racelen and self.cater.position >= self.racelen:
         print("Tie")
      elif self.slug.position >= self.racelen:
         print(self.slug.winMsg)
      elif self.cater.position >= self.racelen:
         print(self.cater.winMsg)
      else:
         print("Something has gone terribly wrong.")
      
   def printRace(self):
      for i in range(1, self.racelen):
         if i == self.slug.position and i == self.cater.position:
            print("X", end='')
         elif i == self.slug.position:
            print(self.slug.symbol, end='')
         elif i == self.cater.position:
            print(self.cater.symbol, end='')
         else:
            print(" ", end='')
         
      print("|", end='\r')
         
            