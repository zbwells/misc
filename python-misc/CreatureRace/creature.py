## Creature object which contains position, which every creature needs
## Jul 17 2022
## zbwells <zbwells@protonmail.com>

'''Python implementation of Benjamin Vipperman's CreatureRace program, written for Dr. Jacob Somervelle's CSC 1180 (Programming in C++) course at The University of Virginia's College at Wise.'''

class Creature:
   def __init__(self, p = 1, w = False):
   
      # Dummy starting attributes
      self.position = 1
      self.won = 0
      
      # Placeholder creature attributes
      self.winMsg = "Game End"
      self.symbol = "?"