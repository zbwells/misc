#!/usr/bin/env python3

# Script which runs the race when executed
# Jul 17 2022
# zbwells <zbwells@protonmail.com>

if __name__ == "__main__":
   from race import Race
   
   # Race(length, time constant)
   racing = Race(100, 0.2)
   
   try:
      racing.go()
   except KeyboardInterrupt:
      print()
      
   exit(0)