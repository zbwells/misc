#!/usr/bin/env python3

from random import randint
from subprocess import call

## Opens dictionary and randomly chooses a word.
def fetch_word(filename):
    words = [word[:-1] for word in open(filename)]
    return words[randint(0, len(words))-1]

## Queries guess from user, alphabetizes, and checks against solution.    
def main():
    word = fetch_word("dictionary")
    before = []
    after = []

    call("clear")
    while True:
        guess = str(input("Enter your word guess: "))

        if guess < word:
            before.append(guess)
            before = sorted(before)
        elif guess > word:
            after.append(guess)
            after = sorted(after)
        else:
            print("weiner")
            break

        call("clear")

        # Informative text interface things
        print("Before the word: ")
        for string in before:
            print(string)
        
        print("-------------------------")
        print("After the word: ")
        for string in after:
            print(string)

        print()

try:
    # Only runs the script if it is called independently.
    if __name__ == "__main__":
        main()
except (KeyboardInterrupt, EOFError):
    exit()
    

    
