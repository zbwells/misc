## Room full of monkeys with typewriters try to rewrite the works of Shakespeare, or whatever you give it, actually. A crisis waiting to unfold. Unleash this on your friends or something, I don't know.

# Distributed as free software under CC0, see LICENSE for more information.

## zbwells <zbwells@protonmail.com>
## Jul 13 3:35 PM

from sys import stderr, stdin
from random import randint
from multiprocessing import Process
from os import cpu_count
import argparse
import enchant

'''In practice, this is a multi-threaded random character generator that will spit out english words once the proper character sequences just happen to be generated. I might implement some kind of neural network at some point instead of randomness, but not right now.'''

# TypeWriter, which has keys the Monkey can type on.
# Check an ASCII table for clarification on the contents of the keys list.
class TypeWriter:
   def __init__(self):
      self.keys = list(range(97, 123))
      self.keys = list(map(chr, self.keys))
      self.keylen = len(self.keys)
      
# Monkey, which when given a typewriter, will immediately start typing randomly.
class Monkey:
   def __init__(self, device):
      self.device = device
      
   def pressKey(self):
      randomkey = randint(0, self.device.keylen-1)
      return self.device.keys[randomkey]

# Playwright, who determines whether or not what the Monkey types is a word or not.
class Playwright:
   def __init__(self):
      self.dictionary = enchant.Dict("en_US")
      self.letters = ""
      self.lettercount = 0
      self.bias = self.letter_length_bias()
      self.words = []
   
   # Avoid single letters only sometimes and occasionally give preference to larger words
   def letter_length_bias(self):
      bias = 0
      while True:
         if bias > 5:
            bias = 1
         else:
            bias += 1
            
         yield bias
   
   # Give the Playwright a letter, when he receives enough, he will check for words   
   def giveLetter(self, ch):
      self.letters += ch
      self.lettercount += 1
   
      if len(self.letters) >= 30:
         for i in range(len(self.letters)):
            for k in range(i+next(self.bias), len(self.letters)):
               string = "".join(self.letters[i:k])
               if self.dictionary.check(string) == True:
                  self.words.append(string)
               string = ""
               
         self.letters = ""
         

# Now we shall put them all in a room and see what happens.
class Room:
   def __init__(self, target, arguments):
      self.word_found = False
      self.shakespeare = Playwright();
      self.selectric = TypeWriter();
      self.bob = Monkey(self.selectric);
      self.validkeys = self.bob.device.keys
      self.target = self.sanitize_input(target)
      self.args = arguments
      self.monkeys = [] 
      self.complete = self.work()
   
   # Take the target input and remove invalid characters
   def sanitize_input(self, target):
      temp_target = []
      for word in target:
         new_word = "".join([ch.lower() for ch in word if ch.lower() in self.validkeys])
         
         if self.shakespeare.dictionary.check(new_word) != True:
            print("The target is not proper!", file=stderr)
            exit(0)
            
         temp_target.append(new_word)
      
      return temp_target             
   
   def print_stats(self):
      wordnum = len(self.shakespeare.words)
      letternum = self.shakespeare.lettercount
      current_letters = "".join(self.shakespeare.letters)   
      format_string = "Words: %i\t\tLetters: %i\t\tCurrent Letters: %s       "
      formatted_string = format_string % (wordnum, letternum, current_letters)
      print(formatted_string, end='\r')
   
   def work(self):
      test_case = lambda : {(word in self.shakespeare.words) for word in self.target}
      
      def write():
         while test_case() != {True}:
            self.shakespeare.giveLetter(self.bob.pressKey())
            self.print_stats()
         
      try:
         for i in range(int(*self.args.monkey_number)):
            self.monkeys.append(Process(target=write))
         for monkey in self.monkeys:
            monkey.start()
         
         breakvar = False
         while True:
            for monkey in self.monkeys:
               if monkey.is_alive() == False:
                  breakvar = True    
            if breakvar:
               break
          
         for monkey in self.monkeys:
            monkey.terminate()     
               
         print() 
      except KeyboardInterrupt:
         print()
         print("The monkeys were interrupted...")
         for monkey in self.monkeys:
            monkey.terminate()
         exit(0)
      else:
         print("Finish!")

# Handles arguments or something like that
class Moderator:
   def __init__(self):
      self.parser = argparse.ArgumentParser()
      self.parser.add_argument("--monkeys", type=int, nargs=1, default=[1],
         help="number of monkeys with typewriters (number of processes started)",
         dest="monkey_number")
      
      self.parser.add_argument("--target", type=str, nargs=1, default="",
         help="target word(s) for monkeys to write, uses stdin if not provided",
         dest="target_words")
      
      self.args = self.parser.parse_args()
      

# If not being imported as a module, put monkeys and typewriters in a room.         
if __name__ == "__main__":
   # use sum() to flatten list of lists of split strings
   parsed_args = Moderator().args
   
   if parsed_args.target_words == "":
      target_words = sum([line.split(' ') for line in stdin], [])
   else:
      target_words = parsed_args.target_words
      
   infinite_monkey_theorem = Room(target_words, parsed_args) 
   
   
         
      
          
         
      
      
   
       




