// headers for the argument handling code
// zbw - zbw9uc@uvawise.edu

#pragma once

// struct to contain the options and the order in which they come, -1 if they don't exist
typedef struct {
	int file_usage;
	int help;
	int words;
	int lines;
	int chars;
} args_data;


// Counts the number of files to run calculations on
int  	count_files(int argc, char *const *argv);

// Initialize the struct containing argument flags
void 	args_data_init(args_data *args);

// Handle the arguments -- that is, actually put the correct values into the struct
void 	handle_arguments(args_data *args, int argc, char *const *argv);

 
