// mini wc remake thing

// Jan 1 2022
// zbw - zbw9uc@uvawise.edu

// TODO: figure out how to make manpages (i.e. "man zwc")

// include standard io library and our main headers
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

// include program-specific headers, written by program author
#include "main.h"
#include "args.h"
#include "help.h"

void file_data_init(file_data *data) {
	data->lines = 0;
	data->words = 0;
	data->chars = 0;
}

// Currently calculates all data regardless of whether the user asks for it or not.
// Probably a better way to do this.
void populate_data_struct(file_data *data, FILE *fp) {
	char ch;
	int temp = 0;
	while ( ( ch = fgetc(fp) ) != EOF ) {
		if ( ch == '\n' ) {
			data->lines += 1;
			temp = 0;
		}
		else if ( ch == ' ' || ch == '\t' ) {
			temp = 0;
		}
		else if ( temp == 0 ) {
			temp = 1;
			data->words += 1;
		}
			
		data->chars += 1;
	}
}

void print_total(file_data *data, args_data *args) {
	if ( args->words == -1 && args->lines == -1 && args->chars == -1 ) {
		printf("%4u %4u %4u ", data->lines, data->words, data->chars, data->filename);
	}
	else {
		if ( args->words != -1 )
			printf("%4u ", data->words);
		if ( args->lines != -1 )
			printf("%4u ", data->lines);
		if ( args->chars != -1 )
			printf("%4u ", data->chars);
	}
	
	printf("%4s\n", data->filename);
}

// For some reason the output is different when piping input from stdin? (in the original wc)
// I can't see any reason for this behaviour, so I might change it or maybe ask
void count_from_stdin(file_data *data, args_data *args) {
	populate_data_struct(data, stdin);
	
	if ( args->words == -1 && args->lines == -1 && args->chars == -1 ) {
		printf("%7u %7u %7u", data->lines, data->words, data->chars);
	}
	else {
		if ( args->words != -1 )
			printf("%7u ", data->words);
		if ( args->lines != -1 )
			printf("%7u ", data->lines);
		if ( args->chars != -1 )
			printf("%7u ", data->chars);
	}
	
	printf("\n");
	
}

void count_from_file(file_data *data, args_data *args) {
	FILE *fp;
	
	if ( !( fp = fopen(data->filename, "r") ) ) {
		fprintf(stderr, "zwc: %s: No such file or directory\n", data->filename);
		exit(1);
	}
	
	populate_data_struct(data, fp);
	print_total(data, args);
	
	fclose(fp);
}

int main(int argc, char **argv) {
	int filecount = count_files(argc, argv);
	
	args_data arg;
	args_data *args = &arg;
	args_data_init(args);
	
	// Make all the argument flags happen
	handle_arguments(args, argc, argv);
	
	// Print help if called
	if ( args->help == 1 ) {
		print_help_message();
		return 0;
	}
	
	// Get data from stdin if there are no filenames...
	if ( args->file_usage < 0 ) {
		file_data data;
		count_from_stdin(&data, args);
	} 
		
	// Otherwise generate the structs with all the filenames
	else {
		file_data data[filecount+1];
		for ( int i = 0; i < filecount; i++ ) {
			file_data_init(&data[i]);
		}
	
		// insert filenames, populate all the stuff in all the structs for all the files
		int temp_count = 0;
		for ( int a = 1; a < argc; a++ ) {
			if ( argv[a][0] != '-' ) {
				data[temp_count].filename = argv[a];
				count_from_file(&data[temp_count], args);
				temp_count++;
			}	
		}
		
		if ( filecount > 1 ) {
			file_data total;
			file_data_init(&total);
			for ( int i = 0; i < filecount; i++ ) {
				total.filename = "total";
				total.lines += data[i].lines;
				total.words += data[i].words;
				total.chars += data[i].chars;
			}
			
			print_total(&total, args);
		}
	}
	
	return 0;
}
	
	
	
	
