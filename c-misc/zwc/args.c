/* wc program from GNU coreutils, redesigned potentially to test the viability of it as an assignment to give to students */

// Argument handling routines

// Jan 3 2022
// zbw - zbw9uc@uvawise.edu

#include <stdio.h>
#include <string.h>

#include "args.h"

// Struct to determine which options are utilized
void args_data_init(args_data *args) {
	args->file_usage = -1;
	args->help = -1;
	
	// The eventual value of these determines in which order they come
	args->words = -1;
	args->lines = -1;
	args->chars = -1;
}

// Loop through the arguments one time to get how many files there are
int count_files(int argc, char *const *argv) {
	int count = 0;
	for ( int a = 1; a < argc; a++ ) {
		if ( argv[a][0] != '-' ) {
			count++;
		}
	}
	
	return count;
}

// Populates the args_data struct
void handle_arguments(args_data *args, int argc, char *const *argv) {
	for ( int a = 1; a < argc; a++ ) {
		if ( strcmp(argv[a], "-h") == 0 || strcmp(argv[a], "--help") == 0 ) {
			args->help = 1;
			return;
		}
		if ( argv[a][0] != '-' ) {
			args->file_usage = 1;
		}
		if ( strcmp(argv[a], "-w") == 0 || strcmp(argv[a], "--words") == 0 )  {
			args->words = 1;
		}
		if ( strcmp(argv[a], "-l") == 0 || strcmp(argv[a], "--lines") == 0 )  {
			args->lines = 1;
		}
		if ( strcmp(argv[a], "-c") == 0 || strcmp(argv[a], "--chars") == 0 )  {
			args->chars = 1;
		}
	}	
}
			

