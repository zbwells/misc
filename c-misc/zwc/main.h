// Headers for main.c
// zbw - zbw9uc@uvawise.edu

#pragma once
#include "args.h"

// struct to contain data about the file
typedef struct {
	unsigned int lines;
	unsigned int words;
	unsigned int chars;
	char *filename;
} file_data;

// Initialize the values in the file data struct
void 	file_data_init(file_data *data);

// Populate the struct with reasonable values, aka actually calculate the word count, line count, etc.
void 	populate_data_struct(file_data *data, FILE *fp);

// Print out the values calculated by populate_data_struct(), with formatting
void	print_total(file_data *data, args_data *args);

// Get values from stdin, so output can be piped into the program via shell shenanigans
void 	count_from_stdin(file_data *data, args_data *args);

// Get values from a file... which includes file opening and closing stuff
void	count_from_file(file_data *data, args_data *args);
