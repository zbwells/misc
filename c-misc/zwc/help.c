/* wc program from GNU coreutils, redesigned potentially to test the viability of it as an assignment to give to students */

// Help file.

// Jan 8 2022
// zbw - zbw9uc@uvawise.edu

#include <stdio.h>

void print_help_message() {
	printf("Usage: zwc [OPTION]... [FILE]...\n\nPrint lines, words, and character counts for each FILE, and a total line if more than one FILE is specified. This order is maintained regardless of the order in which options are provided. A word is a non-zero-length sequence of printable characters delimited by whitespace. \n\nIf no FILE is specified, input is taken from stdin.\n\nOptions: \n\t -c,  --chars \t\t\tprint the character count\n\t -l, --lines \t\t\tprint the line count\n\t -w, --words \t\t\tprint the word count\n\t -h, --help \t\t\tprint this help message\n\nzwc is a GNU Coreutils wc clone program intended for student/study/practice usage.\nAuthor: Z. Wells, Spring 2022 UVA Wise CSC Tutor\nEmail: zbw9uc@uvawise.edu\n");
}

// surely this could be formatted better in the future.
// ~ zbw

