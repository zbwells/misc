#include <stdio.h>
#include <stdlib.h>

/* bubble sort implementation in C, nothing special, refresher
 * zbwells 2025 jan
 */

int swap(int *p1, int *p2)
{
    int tmp = *p1;
    *p1 = *p2;
    *p2 = tmp;
}

/* comparator */
int cmpgr(const void *p1, const void *p2) {
    return *(const int *) p1 > *(const int *) p2;
}

int cmple(const void *p1, const void *p2) {
    return *(const int *) p1 > *(const int *) p2;
}

/* sort lst */
void bubbasort(
    void *base, 
    size_t nmemb,
    size_t size, 
    __compar_fn_t cmp
) { 
    short complete = 0;
    size_t member = 0;
    
    while (1) {
        void *p1 = base + (member * size);
        void *p2 = base + ((member+1) * size);
                 
        if (cmp(p1, p2)) {
            swap(p1, p2);
            complete = 0;
        }

        member++;
        if (member+1 >= nmemb) {
            member = 0;

            if (complete) {
                break;
            }           
            
            complete = 1;
        }
    } 
    
    return;    
}

int main(void)
{
    int lst[] = {5, 3, 4, 1, 2};

    printf("{ ");
    for (int i = 0; i < sizeof(lst) / sizeof(int); i++) {
        printf("%i ", lst[i]);
    }
    printf("}\n");
    
    bubbasort(&lst, sizeof(lst) / sizeof(int), 
                sizeof(int), cmpgr);
    
    printf("{ ");
    for (int i = 0; i < sizeof(lst) / sizeof(int); i++) {
        printf("%i ", lst[i]);
    }
    printf("}");
}
