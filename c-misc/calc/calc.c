/* 
 * Implementation of a single-stack based postfix calculator in C.
 * zbwells
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>

typedef struct node {
    int num;
    struct node *next;
    struct node *previous;   
} node;

typedef struct stack {
    struct node *top;
} stack;


/* Push an integer onto the stack */
void push(stack *nums, int dat)
{
    node *temp = (node *)malloc(sizeof(node));
    temp->num = dat;
    temp->next = NULL;

    if (nums->top == NULL) {
        temp->previous = NULL;
        nums->top = temp;
    } else {
        temp->previous = nums->top;
        nums->top->next = temp;
        nums->top = temp;
    }
}

/* Pop an integer off of the stack and return it's value */
int pop(stack *nums)
{
    int popped = 0;
    
    if (nums->top == NULL) {
        fprintf(stderr, "* Empty stack");
        return 0;
    } else {
        popped = nums->top->num;
        free(nums->top);
        nums->top = nums->top->previous;
    }

    return popped;
}

/* Peek at the integer on top of stack, but don't pop it off */
int peek(stack *nums)
{
    if (nums->top == NULL) {
        fprintf(stderr, "* Empty stack\n");
        return 0;
    }

    return nums->top->num; 
}

/* Clear the stack. */
void clear(stack *nums)
{
    nums->top = NULL;
}

int main(int argc, char **argv)
{   
    stack *ints = (stack *)malloc(sizeof(stack));
    size_t len = 0;
    char *input;
    char *word;

    while (getline(&input, &len, stdin) > 0) {
        word = strtok(input, " ");
        
        do {
            switch (word[0]) {
                case 'p':
                    printf("%i\n", peek(ints));
                    break;
                case 'c':
                    clear(ints);
                    break;
                case 'q':
                    exit(EXIT_SUCCESS);
                    break;
                case '+':
                    push(ints, (pop(ints) + pop(ints)));
                    break;
                case '-':
                    push(ints, (-pop(ints) + pop(ints)));
                    break;
                case '*':
                    push(ints, (pop(ints) * pop(ints)));
                    break;
                case '/':
                    push(ints, (pop(ints) / pop(ints)));
                    break;
                case '0':
                    push(ints, 0);
                    break;
                case '\n':
                    break;
                default:
                    if (atoi(word) != 0) {
                        push(ints, atoi(word));   
                    } else {
                        fprintf(stderr, "%c: unimplemented\n", word[0]);
                    }        
                    break;
            }
        } while ((word = strtok(NULL, " ")));
    }

    free(ints);
    exit(EXIT_SUCCESS);    
}
