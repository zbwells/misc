// Code to actually determine the Slug's behaviour during the race.

// Z.W
// Aug 1 2022

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "slug.h"

// Tells the slug how to move properly
int slug_move(int position) {
   srandom(time(NULL));
   int perc = random() % 100;
   
   if ( perc <= 20 ) {
      return position - 3;
   }
   else if ( perc <= 50 ) {
      return position + 3;
   }
   else if ( perc <= 65 ) {
      return position - 1;
   }
   else {
      return position + 1;
   }
}

// Creates a slug_creature struct with properly initialized variables
slug_creature init_slug(void) {
   slug_creature slug;
   slug.won = 0;
   slug.position = 1;
   slug.move = &slug_move;
   return slug;
}
