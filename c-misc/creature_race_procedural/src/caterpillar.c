// Code to actually determine the Caterpillar's behaviour during the race.

// Z.W
// Aug 1 2022

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "caterpillar.h"

// Caterpillar's movement patterns
int caterpillar_move(int position) {
   srandom(time(NULL));
   int perc = random() % 100;
   
   if ( perc <= 10 ) {
      return position;
   }
   else if ( perc <= 30 ) {
      return position + 6;
   }
   else if ( perc <= 50 ) {
      return position - 6;
   }
   else if ( perc <= 80 ) {
      return position + 2;
   }
   else {
      return position - 1;
   }
}

// Creates a cater_creature struct with properly intialized variables
cater_creature init_caterpillar(void) {
   cater_creature caterpillar;
   caterpillar.won = 0;
   caterpillar.position = 1;
   caterpillar.move = &caterpillar_move;
   return caterpillar;
}