// Race-- puts the slug and caterpillar in a race together

#include <stdio.h>
#include <unistd.h>
#include "caterpillar.h"
#include "slug.h"

#define GAME_LENGTH 80
#define WAIT_TIME_IN_MICROSECONDS 750000

int main(void) {
   slug_creature slug = init_slug();
   cater_creature cater = init_caterpillar();
   
   while ( !slug.won && !cater.won ) {
   
      // Change positions and check positions
      slug.position = slug.move(slug.position);
      cater.position = cater.move(cater.position);
      
      if ( slug.position >= GAME_LENGTH ) {
         slug.won = 1;
      }
      else if ( slug.position < 1 ) {
         slug.position = 1;
      }
      
      if ( cater.position >= GAME_LENGTH ) {
         cater.won = 1;
      }
      else if ( cater.position < 1 ) {
         cater.position = 1;
      }
      
      // Print out game display
      usleep(WAIT_TIME_IN_MICROSECONDS);
      for ( int i = 1; i < GAME_LENGTH; i++ ) {
         if ( i == slug.position && i == cater.position ) {
            printf("X");
         }
         else if ( i == slug.position ) {
            printf("S");
         }
         else if ( i == cater.position ) {
            printf("C");
         }
         else {
            printf(" ");
         }
      }
      
      printf("|\r");  
      fflush(stdout); 
   }   
   
   // Print out win messages
   if ( slug.won && cater.won ) {
      printf("Tie\n");
   }
   else if ( slug.won ) {
      printf("Slug Wins!\n");
   }
   else if ( cater.won ) {
      printf("Caterpillar Wins!\n");
   }
   
   return 0;
}


      
      
      
