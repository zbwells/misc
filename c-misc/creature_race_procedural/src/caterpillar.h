// Structure template to determine the Caterpillar's behaviour during the race.

// Z.W.
// Aug 1 2022

#pragma once

typedef struct creature2 {
   int won;
   int position;
   int (*move)(int);
} cater_creature;

cater_creature init_caterpillar(void);