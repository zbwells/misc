// Structure template to determine the Slug's behaviour during the race.

// Z.W.
// Aug 1 2022

#pragma once

typedef struct creature {
   int won;
   int position;
   int (*move)(int);
} slug_creature;

slug_creature init_slug(void);
