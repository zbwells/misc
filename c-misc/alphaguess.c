#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

/*
 * alphaguess, but in C
 * zbwells dec 24 2024  
 *
 */

#define BUFLEN 256

/* Fetches word from dictionary (uses roulette wheel) */
void fetch_word(char *restrict dst, char *filename) {
    FILE *fp = fopen("dictionary", "r");
    short randint = 0;
    int counter = 0;
    int ch = '\0';
    
    srandom(time(NULL));

    /* 1% chance to stop on a word */
    while (randint < 99) {
        ch = fgetc(fp);

        if (ch == '\n') {
            randint = random() % 101;        
        } else if (ch == EOF) {
            rewind(fp);
        }
    }

    /* yoink the word and place it in the buffer */
    while (isalpha((ch = fgetc(fp)))) {
        if (ch == EOF) {
            rewind(fp);
            ch = fgetc(fp);
        }

        *dst = ch;
        
        dst++;  
        counter++;

        if (counter >= BUFLEN-1) {
            fprintf(stderr, "The word from dictionary is too big!");    
        }       
    } 
}

/* Queries the user to guess a word */
void fetch_guess(char *restrict dst) {
    int counter = 0;
    char ch;

    while ((ch = getchar()) != '\n' && counter < BUFLEN-1) {
        *dst = ch;
        dst++;
        counter++;
    }

    *dst = 0;            
}

/* Taken from Linux man-pages 6.03 qsort example (callback for qsort). */
static int cmpstring(const void *p1, const void *p2) {
    return strcmp(p1, p2);    
}

/* Takes the list of already guessed words, puts them in alpha order */
void alpha_order(char *buf) {
    char arr[BUFLEN][BUFLEN] = {0};
    char word[BUFLEN] = {0};
    char *wordptr = word;
    char *bufptr = buf;

    int arrcount = 0;
    while (*bufptr) {
        if (*bufptr == '\n') {
            *wordptr = '\0';
            strncpy(arr[arrcount], word, strlen(word));
            wordptr = word;
            arrcount++;
        } else {
            *wordptr = *bufptr;
            wordptr++;      
        }

        bufptr++;      
    }
    
    qsort(&arr, arrcount, sizeof(arr[0]), cmpstring);
    
    for (int i = 0; i < arrcount; i++) {
         for (int j = 0; j < strlen(arr[i]); j++) {
            *buf = arr[i][j];
            buf++;
         }

        *buf = '\n';
        buf++;
    }
}

int main(void) {
    char wordbuf[BUFLEN] = {0};
    char guessbuf[BUFLEN] = {0};
    char abovebuf[BUFLEN] = {0};
    char belowbuf[BUFLEN] = {0};
    
    char *dict = "dictionary";
    char *word = wordbuf;
    char *above = abovebuf;
    char *below = belowbuf;
    char *guess;
    
    fetch_word(wordbuf, dict);

    system("clear");
    /* printf("Debug - Answer: %s\n", word); */
    
    while (word != guess) {   
        printf("Enter a guess: ");    
        fetch_guess(guessbuf);    
        guess = guessbuf;

        int comparison = strcasecmp(word, guess);
        if (comparison == 0 && strcmp(word, "\n") != 0) {
            puts("weiner");
            break;
        } else if (comparison > 0) {
            printf("%i\n", strlen(guess));
            strncat(guess, "\n", 2);
            strncat(above, guess, strlen(guess)); 
        } else if (comparison < 0) {
            strncat(guess, "\n", 2);
            strncat(below, guess, strlen(guess));
        }

        system("clear");
        
        puts("Above: ");
        if (abovebuf[0] != '\0' && strcmp(guess, "\n")) { 
            alpha_order(abovebuf); 
        }
        
        printf("%s", abovebuf);
        
        puts("-----------");
        
        puts("Below: ");
        if (belowbuf[0] != '\0' && strcmp(guess, "\n")) {
            alpha_order(belowbuf); 
        }
        
        printf("%s", belowbuf);
    }
    
    return 0;
}
