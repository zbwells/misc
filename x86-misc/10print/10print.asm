; 10print but in x86 assembly because that seems like a great idea

SECTION .data
forward     db      '╱', 0x0
reverse     db      '╲', 0x0

SECTION .bss
randbuf     resb 1          ; set up buffer with length of 1 to hold our random byte

SECTION .text
global _start

_start:
getrnd: 
    mov     eax, 0x163      ; getrandom syscall
    mov     ebx, randbuf    ; set the buffer that the random byte is landing in
    mov     ecx, 1          ; make sure it only gets 1 byte
    mov     edx, 0          ; flag 0, probably corresponds to /dev/urandom
    int     0x80            ; syscall interrupt

pickch:     
    mov     eax, forward    ; move / into accumulator
    mov     edx, [randbuf]  ; move the random byte into edx
    cmp     edx, 128        ; compare the random byte with 128
                            ; byte can be between 0-255, so 50-50 odds of either choice
    jle     writec          ; if less than 128, write / (which is already in eax)
    mov     eax, reverse    ; else, write \

writec:
    mov     ecx, eax        ; move string into ecx
    mov     edx, 3          ; use 3 bytes for printing-- unicode 2 bytes + null terminator
    mov     ebx, 1          ; set stdout as output file
    mov     eax, 4          ; setup write syscall 
    int     0x80            ; syscall interrupt
    jmp     getrnd          ; infinite loop
