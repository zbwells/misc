#!/usr/bin/env bash

# meant for x86_64 systems but written for x86 systems

test "-d" == "$1" && {
    nasm -f elf -g -F dwarf 10print.asm;
} || {
    nasm -f elf 10print.asm;   
}

ld -m elf_i386 10print.o -o 10print;
