; isprime.asm
; Takes one integer argument and determines if it is prime or not.
; zbwells
; Jun 22 2023

SECTION .data
few_args_msg    db         'Usage: isprime <integer>', 0xA, 0x0
yes_msg         db         'The number is prime.', 0xA, 0x0
no_msg          db         'The number is not prime.', 0xA, 0x0

SECTION .text
global _start

_start:
    pop     ecx            ; put number of args into ecx
    cmp     ecx, 1         ; if number of args is only 1
    je      .too_few_args  ; quit and say why
    pop     eax            ; pop first argument off stack
    pop     eax            ; second argument is the one we care about
    call    atoi           ; convert string into integer value
    
    call    isprime        ; call isprime with result of atoi    
    cmp     eax, 1         ; if the number is prime
    je      .print_yes     ; print yes and exit

    cmp     eax, 0         ; if the number is not prime
    je      .print_no      ; print no and exit

 .print_yes:
    mov    eax, yes_msg   ; prepare yes_msg to be sent
    jmp    .done

 .print_no:
    mov    eax, no_msg    ; prepare no_msg to be sent
    jmp    .done         
     
 .too_few_args:
    mov     eax, few_args_msg    ; move usage string into eax

 .done:
    call    sprint      ; print out selected string
    call    exit        ; terminate program

exit:
    xor     ebx, ebx    ; zero out ebx register which will be used as return value
    mov     eax, 1      ; prepare exit syscall
    int     0x80        ; syscall interrupt
    ret

print_newline:
    push    eax            ; preserve eax for later use
    mov     eax, 0xA       ; move a newline into eax
    push    eax            ; put eax on top-of-stack so we can get addr of newline
    mov     eax, esp       ; get address of newline (esp points to top-of-stack)
    call    sprint         ; print out newline
    pop     eax            ; remove the newline from stack
    pop     eax            ; return eax back to original status
    ret
    
strlen:
    push    ebx         ; preserve ebx for later use
    mov     ebx, eax    ; move eax into ebx

 .strlen_nextchar:
    cmp     byte [eax], 0       ; check if value pointed to by eax is a null byte
    jz      .strlen_endlen      ; if it is, terminate loop
    inc     eax                 ; else, increment eax so it points to the next char
    jmp     .strlen_nextchar    ; loop

 .strlen_endlen:
    sub     eax, ebx    ; subtract initial position from current position
    pop     ebx         ; reset ebx to initial value
    ret

sprint:
    push    edx        ; 
    push    ecx        ; push the values of these registers onto stack 
    push    ebx        ; so they can be reset later, eax = the string
    push    eax        ;
    call    strlen         

    mov     edx, eax    ; move the result of strlen (length of string) into edx
    pop     eax         ; return eax to original value (which is the string)

    mov     ecx, eax   ; move the string into ecx 
    mov     ebx, 1     ; stdout goes into ebx 
    mov     eax, 4     ; load syscall number for write syscall into eax
    int     0x80       ; syscall interrupt

    pop     ebx        ; reset ebx to original value
    pop     ecx        ; reset ecx to original value
    pop     edx        ; reset edx to original value
    ret

atoi:
    push    ebx          ;
    push    ecx          ; put these registers on the stack to be later restored
    push    edx          ; ^
    push    esi          ;
    mov     esi, eax     ; move pointer to number into esi
    xor     eax, eax     ; initialize eax with decimal value 0
    xor     ecx, ecx     ; initialize ecx with decimal value 0

 .atoi_multiply_loop:
    xor     ebx, ebx          ; resets both lower and upper bytes of ebx to be 0
    mov     bl, [esi+ecx]     ; move a single byte into ebx register's lower half
    cmp     bl, 48            ; if number in bl is lower than ascii range...
    jl      .atoi_finished    ; jump to finished 
    cmp     bl, 57            ; if number in bl is higher than ascii range...
    jg      .atoi_finished    ; jump to finished

    sub     bl, 48          ; subtract a hex value out of current ascii byte
    add     eax, ebx        ; add ebx to our integer value in eax
    mov     ebx, 10         ; move decimal value 10 into ebx
    mul     ebx             ; multiply eax by ebx to get place value
    inc     ecx             ; increment ecx (our counter register)
    jmp     .atoi_multiply_loop  

 .atoi_finished:
    cmp     ecx, 0          ; if the counter is zero
    je      .atoi_restore   ; jump (no integer arguments were passed)
    mov     ebx, 10         ; move decimal value 10 into ebx
    div     ebx             ; divide eax by value in ebx (in this case 10)

 .atoi_restore:
    pop     esi     ;
    pop     edx     ; restore all registers from off the stack
    pop     ecx     ; restore all registers from off the stack :)
    pop     ebx     ;
    ret
             
isprime:
    push     ebx          ; variable to store upper limit in
    push     ecx          ; loop counter    
    push     edx          ; for usage in trial divison
    push     esi          ; for relocating testnum

    cmp     eax, 0             ; if eax is zero
    jz      .isprime_false     ; jump to true

    mov     esi, eax    ; relocate test num     
    mov     ecx, 2      ; initialize ecx for the loop (also used for division)
    div     ecx         ; divide eax (our number) via ecx, which is currently 2
    mov     ebx, eax    ; store upper test limit in ebx
    add     ebx, 1      ; floor division plus 1 to make upper bound
    
    cmp     ebx, ecx        ; compare upper limit and loop start
    jle     .isprime_true   ; if upper limit is less than loop start, it's prime
    
 .isprime_trial_loop:
    xor     edx, edx           ; zero out edx
    mov     eax, esi           ; move testnum into eax
    div     ecx                ; divide testnum by counter
    cmp     edx, 0             ; if remainder is zero
    jz     .isprime_false      ; terminate subroutine, return false
    inc     ecx                ; increment counter
    cmp     ecx, ebx           ; compare counter to upper limit
    jne    .isprime_trial_loop
    
 .isprime_true:
    mov    eax, 1           ; move 1 (true) into eax for return value
    jmp    .isprime_end     ; jump to end

 .isprime_false:
    mov     eax, 0           ; move 0 (false) into eax for return value
    jmp     .isprime_end     ; jump to end

 .isprime_end:
    pop     ebx     ; restore ebx to prior value
    pop     ecx     ; restore ecx to prior value
    pop     edx     ; restore edx to prior value
    pop     esi     ; restore esi to prior value
    ret
