#!/usr/bin/env bash

# meant for x86_64 systems but written for x86 systems

test "-d" == "$1" && {
    nasm -f elf -g -F dwarf isprime.asm;
} || {
    nasm -f elf isprime.asm;   
}

ld -m elf_i386 isprime.o -o isprime;

