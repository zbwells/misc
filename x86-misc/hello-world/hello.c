/* 
 * hello.c
 * Uses the write() syscall wrapper from unistd,
 * rather than printf() from stdio.
 * Either probably results in almost identical compiled code, 
 * thanks to GCC being so ruthlessly optimizing.
 * but in my head, this makes for a better comparison to the assembly version.
 */

#include <unistd.h>

int
main(void)
{
	write(1, "hello world\n", 12);
	return 0;
}
