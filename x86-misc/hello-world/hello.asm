; hello.asm 
; must be compiled with nasm, and linked with ld

SECTION .data
msg	db 'hello world', 0xA

SECTION .text
global _start

_start:
				; prepare write syscall
	mov	edx, 12		; number of bytes to write
	mov 	ecx, msg	; move pointer to our string into ecx
	mov 	ebx, 1 		; write to stdout file (1 is stdout file descriptor)
	mov 	eax, 4		; invoke write syscall
	int 	0x80		; syscall interrupt
				; prepare exit syscall
	xor 	ebx, ebx 	; exit syscall
	mov 	eax, 1 		; invoke exit syscall
	int 	0x80		; syscall interrupt
