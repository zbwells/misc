#!/usr/bin/guile -s
!#

; Program from prog lang but in Guile

(use-modules (ice-9 textual-ports))
(use-modules (ice-9 format))

; Decides whether or not to use default filename or another filename
(define (filename)
 	(if (null? (cdr (command-line))) "numbers.dat" (cadr (command-line))))

; Reads file and gets a string containing all the input
(define (read-file! path)
	(call-with-input-file path
		(lambda (port)
			(get-string-all port))))

; Makes sure all numbers in a list of lists are actually numbers
(define (sanitize-input lists)
  (let ((not-null? (lambda (lst) (not (null? lst))))) 
  (filter not-null? (map (lambda (lst) (filter number? lst)) lists))))
 
; Get a list of lists containing numbers; so a matrix
(define (get-matrix-of-numbers str)
	(let* ((str-rows  (string-split str #\newline))
		   (list-rows (map (lambda (str) (string-split str #\space)) str-rows)))
	  (sanitize-input (map (lambda (str) (map string->number str)) list-rows))))

; Figure out whether there are more rows or columns in a matrix
(define (get-greatest-dimension data)
	(let ((col-len (length (car (apply map list data))))
	 	  (row-len (length (car data)))) 
		(if (> row-len col-len) row-len col-len)))

; Calculate sum of rows in a matrix
(define (sum-matrix-rows data)
	(map (lambda (nums) (apply + nums)) data))

; Calculate sum of columns in a matrix
(define (sum-matrix-cols data)
	(sum-matrix-rows (apply map list data)))

; Calculate average of rows in a matrix
(define (avg-matrix-rows data)
	(map (lambda (nums) (/ (apply + nums) (length nums))) data))

; Calculate average of columns in a matrix
(define (avg-matrix-cols data)
	(avg-matrix-rows (apply map list data)))

; Prints a line of numbers with a title
(define (print-stat! name formatcode nums)
	(display name)
	(format #t formatcode nums)
	(newline))

; Take the previous function and run it multiple times with some nice additions
(define (print-stats! data)
	(print-stat! "    Num | " "~{~8d |~}" (iota (get-greatest-dimension data)))
	
	(map (lambda (num) 
		(display "-")) 
		(iota (* 8 (+ 3 (get-greatest-dimension data)))))
	(newline)
	
	(print-stat! "Row Sum | " "~{~8d |~}" (sum-matrix-rows data))
	(print-stat! "Row Avg | " "~{~8,2f |~}" (avg-matrix-rows data))
	(print-stat! "Col Sum | " "~{~8d |~}" (sum-matrix-cols data))
	(print-stat! "Col Avg | " "~{~8,2f |~}" (avg-matrix-cols data))
	
	(format #t "\nTotal Sum: ~d\n" 
		(apply + (sum-matrix-rows data)))
	(format #t "Total Average: ~,2f\n" 
		(/ (apply + (sum-matrix-rows data)) 
		   (* (length data) (length (apply map list data))))))

;; Run all of the functions
(print-stats! (get-matrix-of-numbers (read-file! (filename))))
