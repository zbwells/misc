#!/usr/bin/env racket
#lang racket

; Program from prog lang but in Guile

; Decides whether or not to use default filename or another filename
(define (filename)
 	(if (void? (command-line)) "numbers.dat" (cadr (command-line))))

; Reads file and gets a string containing all the input
(define (read-file! path)
	(call-with-input-file path
		(lambda (port)
			(port->string port))))

; Makes sure all numbers in a list of lists are actually numbers
(define (sanitize-input lists)
  (let ((not-null? (lambda (lst) (not (null? lst))))) 
  (filter not-null? (map (lambda (lst) (filter number? lst)) lists))))
 
; Get a list of lists containing numbers; so a matrix
(define (get-matrix-of-numbers str)
	(let* ((str-rows  (string-split str "\n"))
		   (list-rows (map (lambda (str) (string-split str " ")) str-rows)))
	  (sanitize-input (map (lambda (str) (map string->number str)) list-rows))))

; Figure out whether there are more rows or columns in a matrix
(define (get-greatest-dimension data)
	(let ((col-len (length (car (apply map list data))))
	 	  (row-len (length (car data)))) 
		(if (> row-len col-len) row-len col-len)))

; Calculate sum of rows in a matrix
(define (sum-matrix-rows data)
	(map (lambda (nums) (apply + nums)) data))

; Calculate sum of columns in a matrix
(define (sum-matrix-cols data)
	(sum-matrix-rows (apply map list data)))

; Calculate average of rows in a matrix
(define (avg-matrix-rows data)
	(map (lambda (nums) (/ (apply + nums) (length nums))) data))

; Calculate average of columns in a matrix
(define (avg-matrix-cols data)
	(avg-matrix-rows (apply map list data)))

(define (format-ints nums)
  (if (null? nums) "\n"
      (string-append (~a (car nums) #:align 'right #:width 8)
                     " |" (format-ints (cdr nums)))))

(define (format-dubs nums)
  (if (null? nums) "\n"
      (string-append (~r (car nums) #:min-width 8 #:precision 2)
                     " |" (format-dubs (cdr nums)))))

(define (print-stats! data)
  (display (string-append 
   "    Num | " (format-ints (range 0 (get-greatest-dimension data)))
   
   (list->string (map (lambda (num) #\-) 
                  (range (* 8 (+ 3 (get-greatest-dimension data))))))
   "\n"
                      
   "Row Sum | " (format-ints (sum-matrix-rows data))
   "Row Avg | " (format-dubs (avg-matrix-rows data))
   "Col Sum | " (format-ints (sum-matrix-cols data))
   "Col Avg | " (format-dubs (avg-matrix-cols data))
   
   "\nTotal Sum: " (number->string (apply + (sum-matrix-rows data)))
   "\nTotal Average: " 
   (~r (/ (apply + (sum-matrix-rows data)) (length (apply append data))) #:precision 2)))
   (newline))
   
;; Run all of the functions
(print-stats! (get-matrix-of-numbers (read-file! (filename))))
