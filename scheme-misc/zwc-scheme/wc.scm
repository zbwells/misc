; Word count command line uitlity based on GNU wc.
; Z. Wells.
; Jan 2023.

; Displays the help page.
(define (print-help-page)
    (display "Usage: zwc [OPTION]... [FILE]...\n\nPrint lines, words, and character counts for each FILE, and a total line if more than one FILE is specified. This order is modified based on the order in which the options are provided. A word is a non-zero-length sequence of printable characters delimited by whitespace. \n\nIf no FILE is specified, input is taken from stdin.\n\nOptions: \n\t -c, --chars \t\t\tprint the character count\n\t -l, --lines \t\t\tprint the line count\n\t -w, --words \t\t\tprint the word count\n\t -h, --help \t\t\tprint this help message\n\nzwc is a GNU Coreutils wc clone program intended for student/study/practice usage.\nAuthor: Z. Wells, Spring 2022 UVA Wise CSC Tutor\nEmail: zbw9uc@uvawise.edu\n"))


; Returns a list of characters from a file
(define (read-until-eof in-port)
    (let ((ch (read-char in-port)))
        (if (eof-object? ch)
            (list)
            (append (list ch) (read-until-eof in-port)))))


; Procedure to count words
(define (count-words lst)
	(if (null? lst)
		(+ 0)
		(if (null? (cdr lst))
		    (+ 1)
		    (if (and (char-whitespace? (car lst))
		        (not (char-whitespace? (cadr lst))))
		        (+ 1 (count-words (cdr lst)))
		        (+ 0 (count-words (cdr lst))))))) 

                
; Check if a character is a newline
(define (newline? char)
    (eq? char #\newline))


; Parse the arguments related to output formatting and print the word counts
(define (print-fields args contents delim)
    (if (not (null? args))
        (begin
            (when (or (equal? "-c" (car args))
                      (equal? "--chars" (car args)))
                (display delim)
                (display (length contents)))
            (when (or (equal? "-l" (car args))
                      (equal? "--lines" (car args)))
                (display delim)
                (display (length (filter newline? contents))))
            (when (or (equal? "-w" (car args))
                      (equal? "--words" (car args)))
                (display delim)
                (display (count-words contents)))
        
        (print-fields (cdr args) contents delim))))


; Parse files and pass them to the print-fields procedure
(define (parse-files args files)
	(if (not (null? files))
		(if (file-exists? (car files))
			(begin 
				(print-fields 
					args 
					(call-with-input-file (car files) read-until-eof) 
					#\space)
				(display (string-append "\t" (car files)))
				(newline)
				(parse-files args (cdr files)))
			(begin
				(display "wc: ")
				(display (car files))
				(display ": No such file or directory")
				(newline)))))
			
       
; Check to see if the help page should be displayed, and if it should, quit
(define (print-help)
	(when (or (member "-h" (command-line))
		      (member "--help" (command-line)))
		(print-help-page)
		(exit))) 


; Returns a list of files present in the command line arguments
(define (make-list-of-files args)
	(if (null? args)
		(list)
		(if (equal? (string-ref (car args) 0) #\-)
			(make-list-of-files (cdr args))
			(append (list (car args)) (make-list-of-files (cdr args))))))


; Decide whether or not to use stdin or cli arg file counting
(define (count-with-file-or-stdin arglst)
	(let ((file-list (make-list-of-files arglst)))
		(if (null? file-list)
			(begin
				(print-fields 
					arglst 
					(read-until-eof (current-input-port))
					#\tab)
				(newline))
			(parse-files arglst file-list))))	


; Decide if default options need to be provided, and run procedures accordingly
(define (run)
	(if (and (not (or 
		        (member "-c" (command-line))
		        (member "--chars" (command-line))))
		     (not (or
		        (member "-w" (command-line))
		        (member "--words" (command-line))))
		     (not (or
		        (member "-l" (command-line))
		        (member "--lines" (command-line)))))
		
		(count-with-file-or-stdin (append '("-l" "-w" "-c") (cdr (command-line))))
		(count-with-file-or-stdin (cdr (command-line)))))

(print-help)
(run)









