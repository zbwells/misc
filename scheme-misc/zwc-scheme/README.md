#### zwc

This is a small clone of GNU wc, a word count program, which emulates its behaviour to some extent, but is incapable of reading certain kinds of files. May or may not work on that.

It is designed to be compatible with both GNU Guile and the Gambit Scheme Compiler (gsc). It may also be compatible with other Scheme implementations, but I haven't tried.

If you want to compile this program for stand-alone usage, and you're using gsc, try something like this:

```
$ gsc -exe -debug -o ./wc wc.scm
```

Or if you don't want debugging symbols,

```
$ gsc -exe -debug -o ./wc wc.scm
```

And then set the executable bit on the binary and run it as if it were any other executable file.

:)


