#!/usr/bin/guile
!#

;; Postfix calculator written in Guile Scheme
;; zbwells
(use-modules (ice-9 readline))

; Prints that the stack is empty.
(define (print-empty)
  (display "postfix: stack empty")
  (newline))

; Takes arguments from cmdline- puts numeric arguments in stack,
; performs math on them if they're numeric, and performs special operations
; when special control codes are parsed.
(define (calc-loop args stack)
  (cond ((null? args) stack)
    ((equal? 'p (car args)) 
     (if (null? stack)
         (begin (print-empty)
                (calc-loop (cdr args) stack))
         (begin (display (car stack))
                (newline)
                (calc-loop (cdr args) stack))))
    ((equal? 'q (car args))
     (exit))
    ((equal? 'c (car args))
     (calc-loop (cdr args) '()))
    ((or (equal? '#{}# (car args)) (equal? '#{ }# (car args)))
     (calc-loop (cdr args) stack))
    ((number? (car args))
     (calc-loop (cdr args) 
                (cons (car args) stack)))
    ((procedure? (car args))
     (if (< (length stack) 2)
         (begin (print-empty)
                (calc-loop (cdr args) stack))
         (calc-loop (cdr args) 
                    (cons ((car args) (cadr stack) (car stack))
                          (cddr stack)))))
    (else (display "'") (display (symbol->string (car args))) (display "' ")
          (display "unimplemented")
          (newline)
          (calc-loop (cdr args) stack))))

; If number, converts from string to number, if operator, convert to procedure
; Else, convert to symbol
(define (convert-input word)
  (let ((operators '("+" "-" "/" "*")))
    (cond ((string->number word) 
           (string->number word))
          ((member word operators)
           (eval (string->symbol word) 
                 (interaction-environment)))
          (else (string->symbol word)))))

; Define the control code symbols, and create the repl for the calculator
(define (control-calc stack)
  (let ((input (readline)))
    (if (eof-object? input)
        (exit))
    (let ((line (map convert-input (string-split input #\space))))
      (control-calc (calc-loop line stack)))))
         
; Start calulator
(control-calc '())      
                
