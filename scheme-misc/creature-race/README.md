### Creature Race in Guile Scheme

There is nothing more entertaining than watching a slug and a caterpillar race across your screen. Idea from an unknown source, but the original version (in C++ with heavy usage of OO features) written by Ben Vipperman for a C++ course final, assigned by at the time MCS Department Chair J.S. Somervell.

I rewrote it in Scheme; I also rewrote it in C, and might rewrite it in Rust too at some point. It's a fun little simple program that doesn't have much to it; so it is pretty fun to sit down for a bit and rewrite it in some other language, especially considering how stupidly entertaining it is to someone like me who is easy to entertain.

To run the program, you need:

GNU Guile 3.0 (2.2 may work, try if you wish)
or some other compatible scheme interpreter/compiler

```
$ guile race.scm
```


