; Race module which puts the slug in the caterpillar in a race against each other
; creature-race

; Built to run with GNU Guile, because it has a sleep procedure

; Z.W.
; Jan 6 2023

; Do this so that the random movement will actaully be random
(set! *random-state* (random-state-from-platform))

(load "creatures/caterpillar.scm")
(load "creatures/slug.scm")

(define game-length 80)
(define wait-time-in-microseconds 500000)

(define (calculate-and-display-race player1 pos1 player2 pos2 trail)
    (cond ((< pos1 1)
             (calculate-and-display-race player1 1 player2 pos2 trail))
          ((< pos2 1)
             (calculate-and-display-race player1 pos1 player2 1 trail))
          ((and (> pos1 game-length) (> pos2 game-length))
             (begin 
                (display "Tie\n") 
                (exit)))
          ((> pos1 game-length)
             (begin 
                (display (cadr (member 'name player1))) 
                (display " Wins!\n") 
                (exit)))
          ((> pos2 game-length)
             (begin 
                (display (cadr (member 'name player2))) 
                (display " Wins!\n") 
                (exit)))
          (else
             (begin 
                (usleep wait-time-in-microseconds)
                (letrec ((print-loop (lambda (i)
                    (if (< i game-length)
                        (begin
                            (cond ((and (= i pos1) (= i pos2))
                                     (display "X"))
                                  ((= i pos1)
                                     (display (cadr (member 'ind player1))))
                                  ((= i pos2)
                                     (display (cadr (member 'ind player2))))
                                  (else
                                     (display " ")))
                            (print-loop (+ i 1)))))))
                    (print-loop 1))
                (if trail (display "|\r") (display "|\n"))
                (force-output)
                (calculate-and-display-race
                    player1 
                    ((cadr (member 'move player1)) pos1)
                    player2
                    ((cadr (member 'move player2)) pos2)
                    trail)))))

(calculate-and-display-race caterpillar-stats 1 slug-stats 1 #f)
                        

