;; Caterpillar module for creature-race

;; Z.W.
;; Jan 6 2023

(define (slug-move position)
    (let ((perc (random 100)))
        (cond ((<= perc 20) (- position 3))
              ((<= perc 50) (+ position 3))
              ((<= perc 65) (- position 1))
              (else (+ position 1)))))

(define slug-stats (list 'name "Slug" 
                         'ind "S" 
                         'move slug-move)) 
