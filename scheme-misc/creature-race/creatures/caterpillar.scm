;; Caterpiillar module for creature-race

;; Z.W.
;; Jan 6 2023

(define (caterpillar-move position)
    (let ((perc (random 100)))
        (cond ((<= perc 10) position)
              ((<= perc 30) (+ position 6))
              ((<= perc 50) (- position 6))
              ((<= perc 80) (+ position 2))
              (else (- position 1)))))

(define caterpillar-stats (list 'name "Caterpillar" 
                                'ind "C" 
                                'move caterpillar-move))
               
                           

