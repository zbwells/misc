module Main where

import System.Environment
import Data.List
import Data.Tuple
import Data.Char

morseCode :: [(Char, String)]
morseCode =  [('a', ".-"),
              ('b', "-..."),
              ('c', "-.-."),
              ('d', "-.."),
              ('e', "."),
              ('f', "..-."),
              ('g', "--."),
              ('h', "...."),
              ('i', ".."),
              ('j', ".---"),
              ('k', "-.-"),
              ('l', ".-.."),
              ('m', "--"),
              ('n', "-."),
              ('o', "---"),
              ('p', ".--."),
              ('q', "--.-"),
              ('r', ".-."),
              ('s', "..."),
              ('t', "-"),
              ('u', "..-"),
              ('v', "...-"),
              ('w', ".--"),
              ('x', "-..-"),
              ('y', "-.--"),
              ('z', "--.."),
              ('1', ".----"),
              ('2', "..---"),
              ('3', "...--"),
              ('4', "....--"),
              ('5', "....."),
              ('6', "-...."),
              ('7', "--..."), 
              ('8', "---.."),
              ('9', "----."),
              ('0', "-----"),
              (' ', "/")]

char2morse :: Char -> String
char2morse str = case lookup (toLower str) morseCode of
                    Just str -> str ++ " "
                    Nothing -> " "

morse2char :: String -> Char
morse2char ch = case lookup ch (map swap morseCode) of
                    Just ch -> ch
                    Nothing -> ' '
                         
str2morse :: String -> String
str2morse str = concat $ map char2morse str

morse2str :: String -> String
morse2str str = map morse2char (words str)

usageString :: String
usageString = "usage: morse -[a|m] text\n\
               \This program can be used to translate\
               \ from ascii text to morse code, or the\
               \ other way around.\n\
               \Options:\n\
               \\t-a | --ascii\t\tTranslate a string of\
               \ English text to Morse code.\n\
               \\t-m | --morse\t\tTranslate a string of\
               \ Morse code to English.\n\
               \\t-h | --help\t\tDisplay this help page.\n\n\
               \Written by zbwells."

parseArgs :: [String] -> String
parseArgs [] = usageString
parseArgs (x:xs) 
    | x == "--morse" || x == "-m" = morse2str $ head xs
    | x == "--ascii" || x == "-a" = str2morse $ head xs
    | x == "--help"  || x == "-h" = usageString
    | otherwise = usageString

main :: IO ()
main = do args <- getArgs
          (putStrLn . parseArgs) args


            
