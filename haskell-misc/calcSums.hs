module Main where

-- Program from prog lang but in Haskell

import Data.List
import Text.Printf
import System.Environment

-- Convenience wrapper over read function to convert from string to integer
strToDigit :: String -> Int
strToDigit str = read str :: Int

-- Wrapper function to change two Ints into Fractionals and output a Double
coerceDivide :: Int -> Int -> Double
coerceDivide x y = fromIntegral x / fromIntegral y

-- Get filename or use numbers.dat if none is provided
getFileName :: [String] -> String
getFileName [] = "numbers.dat"
getFileName names = head names

-- Takes a whole file string and makes a numerical matrix out of it
makeMatrix :: String -> [[Int]]
makeMatrix contents = [map strToDigit (words line) | line <- lines contents] 

-- Calculate sums of all the rows in the matrix
getRowSums :: [[Int]] -> [Int]
getRowSums matrix = map sum matrix

-- Calculate sums of all the columns in the matrix
getColSums :: [[Int]] -> [Int]
getColSums matrix = map sum (transpose matrix)

-- Calculate averages of all the rows in the matrix
getRowAvgs :: [[Int]] -> [Double]
getRowAvgs matrix = 
    map (\num -> coerceDivide num (length (head matrix))) (map sum matrix)

-- Calculate averages of all the columns in the matrix
getColAvgs :: [[Int]] -> [Double]
getColAvgs matrix = getRowAvgs (transpose matrix)

-- Calculate the total sum
getTotalSum :: [[Int]] -> Int
getTotalSum matrix = sum (concat matrix)

-- Calculate the total average
getTotalAvg :: [[Int]] -> Double
getTotalAvg matrix = 
    coerceDivide (sum (concat matrix)) (length (concat matrix)) 

-- Figures out whether there are more rows or more columns and returns that
getGreatestDimension :: [[Int]] -> Int
getGreatestDimension matrix
    | rowLen >= colLen = rowLen
    | otherwise        = colLen
    where rowLen = length (head matrix)
          colLen = length (head (transpose matrix))

-- Figure out how many dashes we should print in the output
-- Based on field width and extra characters
makeLine :: [[Int]] -> String
makeLine matrix = 
    concat ["-" | _ <- [1.. ((getGreatestDimension matrix) + 3) * 8]]

-- Return a string that contains a table of integers of 8 width 
formatInts :: [Int] -> String
formatInts [] = "\n"
formatInts nums = 
    (printf "%8u |" (head nums)) ++ formatInts (tail nums)

-- Same, but for doubles
formatDubs :: [Double] -> String
formatDubs [] = "\n"
formatDubs nums = 
    (printf "%8.2f |" (head nums)) ++ formatDubs (tail nums)

-- Input and output                       
main :: IO ()
main = do
   args <- getArgs
   fileContents <- readFile (getFileName args)
   let numData = makeMatrix fileContents
   
   putStrLn $ 
           "    Num | " ++ formatInts [0.. (getGreatestDimension numData)-1] ++
           makeLine numData ++ "\n" ++
           "Row Sum | " ++ formatInts (getRowSums numData) ++ 
           "Row Avg | " ++ formatDubs (getRowAvgs numData) ++
           "Col Sum | " ++ formatInts (getColSums numData) ++
           "Col Avg | " ++ formatDubs (getColAvgs numData) ++
           
           "\nTotal Sum: " ++ show (getTotalSum numData) ++
           "\nTotal Average: " ++ show (getTotalAvg numData)
