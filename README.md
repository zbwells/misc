# misc

Miscellaneous code written for miscellaneous purposes.

All code within the repository is licensed under the Creative Commons CC0 license. This means the code can be used for any purpose, in any way, for any reason, without permission or credit.
